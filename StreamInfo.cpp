#include "StreamInfo.hh"
#include "Util.hh"

using namespace std;

const string StreamInfo::MESSAGE_TYPE_HEADER = "message_type";
const string StreamInfo::STREAM_INFO_MESSAGE = "stream_info";
const string StreamInfo::STREAM_FPS_HEADER = "fps";
const string StreamInfo::HEIGHT_HEADER = "height";
const string StreamInfo::PACKET_DURATION_HEADER = "packet_duration";
const string StreamInfo::WIDTH_HEADER = "width";
const string StreamInfo::STREAM_URL_HEADER = "url";
const string StreamInfo::STREAM_FORMAT_HEADER = "format";
const string StreamInfo::PROFILE_LEVEL_HEADER = "profile_level";
const string StreamInfo::PACKET_SEQUENCE_HEADER = "sequence_numbers";

void StreamInfo::fill_json_str(string &output) 
{
    util::fill_json_str(output, {
            make_pair(MESSAGE_TYPE_HEADER, STREAM_INFO_MESSAGE),
            make_pair(STREAM_FORMAT_HEADER, this->stream_format),
            make_pair(PROFILE_LEVEL_HEADER, this->profile_level)
            }, {
            make_pair(STREAM_FPS_HEADER, this->stream_fps),
            make_pair(HEIGHT_HEADER, this->height),
            make_pair(WIDTH_HEADER, this->width),
            make_pair(PACKET_DURATION_HEADER, this->packet_duration),
            });

}
