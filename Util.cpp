#include "Util.hh"


void util::fill_json_str(string &output, initializer_list<pair<string, string>> str_arg_list, initializer_list<pair<string, int>> int_arg_list)
{
    output.append("{");
    for (auto it = str_arg_list.begin(); it != str_arg_list.end(); ++it) {
        output.append("\"");
        output.append(it->first);
        output.append("\":\"");
        output.append(it->second);
        output.append("\"");

        if (next(it) != str_arg_list.end() || int_arg_list.size() > 0) {
            output.append(",");
        }

    }

    for (auto it = int_arg_list.begin(); it != int_arg_list.end(); ++it) {
        output.append("\"");
        output.append(it->first);
        output.append("\":");
        output.append(to_string(it->second));

        if (next(it) != int_arg_list.end()) {
            output.append(",");
        }

    }
    output.append("}");

}
