# WebSocketRTSPClient
WebSocketRTSPClient streams an RTSP feed over websocket. It is built off of the live555 testRTSPClient.

2 WebSockets are opened per client for messaging info and for video stream.

Runs on `ws://localhost:8765` by default

## Table of Contents 
  * [Quick Start](#markdown-header-quick-start)
  * [Args](#markdown-header-args)
  * [Client Usage](#markdown-messaging-channel)

## Quick Start
1. Install cmake (required by websocketpp)

        sudo apt install cmake

2. Download and install [boost](https://www.boost.org/doc/libs/1_70_0/more/getting_started/unix-variants.html) (required by websocketpp)

        wget https://dl.bintray.com/boostorg/release/1.68.0/source/boost_1_68_0.tar.bz2
        cd /usr/include
        tar --bzip2 -xf /path/to/boost_1_68_0.tar.bz2

3. Download and install [websocketpp](git://github.com/zaphoyd/websocketpp.git)

        git clone git://github.com/zaphoyd/websocketpp.git
        cd websocketpp
        cmake .
        sudo make install

4. Build and run

        git clone git@bitbucket.org:cswenson_cv/video-player-websocket-server.git
        cd video-player-websocket-server
        make
        ./WebSocketRTSPClient <args> <rtsp-url>

## Args

```bash
-b [SIZE]       sets frame buffer size
-d [LEVEL]      sets debug level between 1-3
-f [N]          sets the number of frames to buffer on
                the websocket server (used only in mp4 or
                elementary stream mode, defaults to 1)
-h              prints this help message
-m              sets fragmented mp4 boxing
-t              sets ts boxing
-p [PORT]       sets the websocket listening port (defaults to 8765)
-s              turns on sequence numbers on the output packets
```

## Client Usage
The messaging channel is the port that clients should connect to (specified by -p), the first message will
be a "video_info" message with the port for video, followed by a "stream_info" message with information about the stream.
