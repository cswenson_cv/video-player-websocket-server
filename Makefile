# Default library filename suffixes for each library that we link with.  The "config.*" file might redefine these later.
libliveMedia_LIB_SUFFIX = $(LIB_SUFFIX)
libBasicUsageEnvironment_LIB_SUFFIX = $(LIB_SUFFIX)
libUsageEnvironment_LIB_SUFFIX = $(LIB_SUFFIX)
libgroupsock_LIB_SUFFIX = $(LIB_SUFFIX)
##### Change the following for your environment:
COMPILE_OPTS = --std=c++11 $(INCLUDES) -I. -DSOCKLEN_T=socklen_t -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64
C =			c
C_COMPILER =		cc
C_FLAGS =		$(COMPILE_OPTS) $(CPPFLAGS) $(CFLAGS)
CPP =			cpp
CPLUSPLUS_COMPILER =	g++
CPLUSPLUS_FLAGS =	$(COMPILE_OPTS) -Wall -DBSD=1 $(CPPFLAGS) $(CXXFLAGS)
OBJ =			o
LINK =			g++ -o
LINK_OPTS =		-L. $(LDFLAGS)
CONSOLE_LINK_OPTS =	$(LINK_OPTS)
LIBRARY_LINK =		ar cr 
LIBRARY_LINK_OPTS =	
LIB_SUFFIX =			a
LIBS_FOR_CONSOLE_APPLICATION = -lboost_system -lpthread
LIBS_FOR_GUI_APPLICATION =
EXE =
##### End of variables to change
WEB_SOCKET_RTSP_CLIENT_OBJS = StreamInfo.$(OBJ) Util.$(OBJ) BroadcastServer.$(OBJ)  WebSocketSink.$(OBJ) WebSocketRTSPClient.$(OBJ) 

WEB_SOCKET_FILE_CLIENT_OBJS = BroadcastServer.$(OBJ) WebSocketFileClient.$(OBJ)

USAGE_ENVIRONMENT_DIR = ./lib/UsageEnvironment
USAGE_ENVIRONMENT_LIB = $(USAGE_ENVIRONMENT_DIR)/libUsageEnvironment.$(libUsageEnvironment_LIB_SUFFIX)

BASIC_USAGE_ENVIRONMENT_DIR = ./lib/BasicUsageEnvironment
BASIC_USAGE_ENVIRONMENT_LIB = $(BASIC_USAGE_ENVIRONMENT_DIR)/libBasicUsageEnvironment.$(libBasicUsageEnvironment_LIB_SUFFIX)

LIVEMEDIA_DIR = ./lib/liveMedia
LIVEMEDIA_LIB = $(LIVEMEDIA_DIR)/libliveMedia.$(libliveMedia_LIB_SUFFIX)

GROUPSOCK_DIR = ./lib/groupsock
GROUPSOCK_LIB = $(GROUPSOCK_DIR)/libgroupsock.$(libgroupsock_LIB_SUFFIX)

TS_MAKER_DIR = ./lib/TSMaker
TS_MAKER_LIB = $(TS_MAKER_DIR)/libtsmaker.a

MP4_MAKER_DIR = ./lib/MP4_lib_maker
MP4_MAKER_LIB = $(MP4_MAKER_DIR)/libmp4encoder.a

LOCAL_LIBS =	$(LIVEMEDIA_LIB) $(GROUPSOCK_LIB) \
		$(BASIC_USAGE_ENVIRONMENT_LIB) $(USAGE_ENVIRONMENT_LIB) $(TS_MAKER_LIB) $(MP4_MAKER_LIB)
LIBS =			$(LOCAL_LIBS) $(LIBS_FOR_CONSOLE_APPLICATION)

INCLUDES = -I$(USAGE_ENVIRONMENT_DIR)/include -I$(GROUPSOCK_DIR)/include -I$(LIVEMEDIA_DIR)/include -I$(BASIC_USAGE_ENVIRONMENT_DIR)/include -I$(TS_MAKER_DIR)/include -I$(MP4_MAKER_DIR)/include -I./include

PREFIX = /usr/local
ALL = WebSocketRTSPClient$(EXE)
all: $(ALL)

.$(C).$(OBJ):
	$(C_COMPILER) -c $(C_FLAGS) $<
.$(CPP).$(OBJ):
	$(CPLUSPLUS_COMPILER) -c $(CPLUSPLUS_FLAGS) $<

$(LIVEMEDIA_LIB):
	cd $(LIVEMEDIA_DIR) && make && cd ../..

$(GROUPSOCK_LIB):
	cd $(GROUPSOCK_DIR) && make && cd ../..

$(BASIC_USAGE_ENVIRONMENT_LIB):
	cd $(BASIC_USAGE_ENVIRONMENT_DIR) && make && cd ../..

$(USAGE_ENVIRONMENT_LIB):
	cd $(USAGE_ENVIRONMENT_DIR) && make && cd ../..

$(TS_MAKER_LIB):
	cd $(TS_MAKER_DIR) && make && cd ../..

$(MP4_MAKER_LIB):
	cd $(MP4_MAKER_DIR) && make && cd ../..

WebSocketRTSPClient$(EXE):	$(WEB_SOCKET_RTSP_CLIENT_OBJS) $(LOCAL_LIBS)
	$(LINK)$@ $(CONSOLE_LINK_OPTS) $(WEB_SOCKET_RTSP_CLIENT_OBJS) $(LIBS)

WebSocketFileClient$(EXE):

clean:
	cd $(LIVEMEDIA_DIR) && make clean && cd ../..
	cd $(GROUPSOCK_DIR) && make clean && cd ../..
	cd $(BASIC_USAGE_ENVIRONMENT_DIR) && make clean && cd ../..
	cd $(USAGE_ENVIRONMENT_DIR) && make clean && cd ../..
	cd $(MP4_MAKER_DIR) && make clean && cd ../..
	-rm -rf *.$(OBJ) $(ALL) core */*.core *~ */include/*~

re: clean all

install: $(ALL)
	  install -d $(DESTDIR)$(PREFIX)/bin
	  install -m 755 $(ALL) $(DESTDIR)$(PREFIX)/bin
