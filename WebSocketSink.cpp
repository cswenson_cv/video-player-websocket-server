
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <stdio.h>
#include <unistd.h>

#include <algorithm>
#include <cerrno>
//#include <chrono>
#include <cstring>
#include <fstream>
#include <iterator>
#include <vector>

#include "Debug.h"
#include "H264_TS_Packetizer.hh"
#include "Mp4Encoder.hh"
#include "Util.hh"
#include "WebSocketRTSPClient.hh"
#include "WebSocketSink.hh"

using websocketpp::connection_hdl;
using websocketpp::frame::opcode::binary;
using websocketpp::frame::opcode::text;

using namespace std;


const unsigned char WebSocketSink::START_CODE[] = {0x00, 0x00, 0x00, 0x01};
const unsigned char WebSocketSink::END_OF_SEQUENCE[] = {0x0a};
const string WebSocketSink::PACKET_INFO = "packet_info";
const string WebSocketSink::SEQUENCE_NUMBER_HEADER = "sequence_number";

void setPts(vector<unsigned char> &message, connection_info *&con_info)
{
    unsigned long pts = con_info->get_pts();
    setBaseMediaDecodeTime(message, pts);
    con_info->increment_pts();
}

WebSocketSink* WebSocketSink::createNew(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId) {
    return new WebSocketSink(env, subsession, streamId);
}

WebSocketSink::WebSocketSink(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId)
    : MediaSink(env),
    fSubsession(subsession) {
        fStreamId = strDup(streamId);
        fReceiveBuffer = new unsigned char[opts.opt_buffsize];
        numFrames = 0;
        iFrameCount = 0;
        packet_num = 0x0;
        headerBuffer = vector<unsigned char>();
        sendChunks = vector<unsigned char>();
        frame_vector = vector<unsigned char>();
        temp_frame_vector = vector<unsigned char>();
        H264Header = vector <char>();
        frame_size_array = vector<struct frameInfo>();


        createWebSocket();
        if (opts.opt_mux) {
            createTSMuxer();
        }
    }


WebSocketSink::~WebSocketSink() {
    m_server->stop();
    delete m_server;
    delete[] fReceiveBuffer;
    delete[] fStreamId;
}



void runWebSocket(BroadcastServer *server) {
};

void WebSocketSink::createWebSocket() {
    string stream_info_json = string();
    stream_info.fill_json_str(stream_info_json);

    if (opts.opt_mux == MP4) {
        m_server = new BroadcastServer(stream_info_json, (TIME_SCALE/stream_info.get_fps()) * stream_info.get_packet_duration());
    } else {
        m_server = new BroadcastServer(stream_info_json);
    }
    m_server->run(opts.opt_port);
}

void WebSocketSink::createTSMuxer() {
    if (lt_init(&sendChunks, stream_info.get_fps()) < 0) {
        cerr << "error creating muxer" << endl;
        exit(EXIT_FAILURE);
    }
}


void WebSocketSink::ts_box(bool ts_headers) {

    if (opts.opt_seq_num) {
        sendChunks.insert(output_vect->begin(), packet_num);
    }
    lt_frame_init();
    if (lt_packetize(frame_vector, frame_size_array, ts_headers) < 0) {
        cerr << "Error packetizing frame" << endl;
        exit(EXIT_FAILURE);
    }

    lt_cleanup();
}

bool WebSocketSink::genMP4Header() {
    int mp4HeaderSize, footerBufSize, size = 0;
    char *footer_buffer;
    unsigned *scratch_table = new unsigned[numFrames * 2];
    unsigned int scratchTableSize = numFrames * 2 * sizeof(unsigned int);
    char errorString[100];
    strcpy(errorString, "");


    if ((mp4HeaderSize = generateMp4Header_w_moov_atom_w_spsppssize(headerBuffer, 0,  iFrameCount, 0, H264Header.size())) == MP4_ERROR) {
        cerr << "Error generating MP4 Header" << endl;
        return false;
        //exit(EXIT_FAILURE);
    }

    footerBufSize = sizeof(char)*(1500 + (12*numFrames*2) + (4*iFrameCount*2));
    footer_buffer = new char[footerBufSize];
    VideoInfo videoInfo = {(int)stream_info.get_width(), (int)stream_info.get_height(), (int)stream_info.get_fps(), 0, frame_size_array.data(), NULL, H264Header.data(), (int)H264Header.size(), iFrameCount > 0 ? (int)numFrames: 0};

    if ((footerBufSize = generateMp4Footer(footer_buffer, scratch_table, scratchTableSize, &size, errorString, &videoInfo, NULL)) == MP4_ERROR) {
        cerr << "Error generating MP4 Header: " << errorString << endl;
        return false;
        //exit(EXIT_FAILURE);
    }

    if (writeMoovAtom(headerBuffer, footer_buffer, footerBufSize, mp4HeaderSize, size) != MP4_OK) {
        cerr << "Error writing moov atom" << endl;
        return false;
        //exit(EXIT_FAILURE);
    }

    delete[] scratch_table;
    delete[] footer_buffer;

    return true;

}


void WebSocketSink::mp4_box()
{
    // add this back in to prepend pts index
    unsigned pts_index = 0;
    // unsigned pts_index_byte_loc;
    //char byteArray[4];
    unsigned packet_num_byte_loc = sendChunks.size();

    VideoInfo videoInfo = {(int)stream_info.get_width(), (int)stream_info.get_height(), (int)stream_info.get_fps(), (int)frame_size_array.size(), frame_size_array.data(), NULL, H264Header.data(), (int)H264Header.size(), iFrameCount > 0 ? (int)numFrames: 0};

    if (opts.opt_seq_num) {
        writePlaceholderBytes(sendChunks, 1);
    }

    // add this back in only if we need to update pts on client
    // pts_index_byte_loc = sendChunks.size();
    //writePlaceholderBytes(sendChunks, 4);

    writeFragment(sendChunks, frame_vector, &videoInfo, NULL, packet_num, &pts_index, NULL);

    if (opts.opt_seq_num) {
        sendChunks[packet_num_byte_loc] = packet_num;
    }

    // add this back in only if we need to update pts on client
    //numToBytes(byteArray, pts_index);
    //copy(byteArray, byteArray + 4, sendChunks.begin() + pts_index_byte_loc);

}

void WebSocketSink::genChunks(unsigned frameSize) {
    // Checking if we've received a 2 byte packet (AUD) since we need to put frames together with the 2 byte packet that appears before a frame
    bool is_aud = (fReceiveBuffer[0] & 0x1F) == 0x9;

    struct frameInfo f_size_info;

    if (is_aud) {
        if (frame_size_array.size() > 0) {
            f_size_info = frame_size_array.back();

            if (f_size_info.IFrame == 1) {

                if (opts.opt_mux == TS) {

                    temp_frame_vector.insert(temp_frame_vector.end(), frame_vector.begin() + f_size_info.startIndex, frame_vector.end());
                    frame_vector.erase(frame_vector.begin() + f_size_info.startIndex, frame_vector.end());

                    frame_size_array.pop_back();

                    sendData();


                    frame_vector.insert(frame_vector.end(), temp_frame_vector.begin(), temp_frame_vector.end());
                    temp_frame_vector.clear();

                    f_size_info.startIndex = 0;
                    frame_size_array.insert(frame_size_array.end(), f_size_info);
                } else if (opts.opt_mux == MP4) {
                    getH264Header(frame_vector.data() + f_size_info.startIndex, H264Header, f_size_info.frameSize);
                    headerBuffer.clear();
                    genMP4Header();
                }

            }

            if (numFrames >= opts.opt_buffer_frames && opts.opt_mux != TS) {
                sendData();
            }
        }

        frame_size_array.emplace_back(frameInfo {(unsigned int)frame_vector.size(), 0, 0});

        numFrames++;
    }

    frame_vector.insert(frame_vector.end(), START_CODE, START_CODE + START_CODE_LEN);

    frame_vector.insert(frame_vector.end(), fReceiveBuffer, fReceiveBuffer + frameSize);

    if (!is_aud && frame_size_array.size() > 0) {
        f_size_info = frame_size_array.back();
        f_size_info.frameSize = frame_vector.size() - f_size_info.startIndex;
        f_size_info.IFrame = is_idr(frame_vector.data() + f_size_info.startIndex) ? 0x1 : 0x0;

        if (f_size_info.IFrame == 1) {
            iFrameCount += 1;
        }
        frame_size_array[frame_size_array.size() - 1] = f_size_info;
    }

}

bool WebSocketSink::is_idr(unsigned char *frame_data)
{
    unsigned char ret = frame_data[10] & 0x1F;
    
    return ret == 0x7 || ret == 0x8;
}

bool WebSocketSink::check_idr_present() 
{
    for (auto it: frame_size_array) {
        if (it.IFrame == 1) {
            return true;
        }
    }

    return false;
}


bool WebSocketSink::sendData() {
    // get frame type
    // if there's an IDR present do processing, send as initial message, otherwise send as regular message

    struct frameInfo f_size_info;
    string seq_num_json = string();

    util::fill_json_str(seq_num_json, {
            make_pair(StreamInfo::MESSAGE_TYPE_HEADER, PACKET_INFO), 
            }, { 
            make_pair(SEQUENCE_NUMBER_HEADER, packet_num)
            });

    f_size_info = frame_size_array.back();


    if (opts.opt_mux == TS) {
        f_size_info.frameSize = f_size_info.frameSize + START_CODE_LEN + END_OF_SEQUENCE_LEN;
        frame_size_array[frame_size_array.size() - 1] = f_size_info;

        frame_vector.insert(frame_vector.end(), START_CODE, START_CODE + START_CODE_LEN);
        frame_vector.insert(frame_vector.end(), END_OF_SEQUENCE, END_OF_SEQUENCE + END_OF_SEQUENCE_LEN);
    }


    if (opts.opt_debug == DBG_LEVEL_3) {
        cout << "Frame vector size: " << frame_vector.size() << endl;
    }

    m_server->broadcast_message<unsigned char, unsigned char>((unsigned char *)seq_num_json.data(), seq_num_json.length(), true, text, false);

    if (opts.opt_mux == TS) {
        ts_box(true);
        m_server->broadcast_message<unsigned char>(sendChunks, sendChunks.size(), false, binary, false);
        sendChunks.clear();
    } else if (opts.opt_mux == MP4) {
        mp4_box();
        m_server->broadcast_message<unsigned char>(sendChunks, sendChunks.size(), false, binary, false, &setPts);
    } else {
        m_server->broadcast_message<unsigned char>(frame_vector, frame_vector.size(), false, binary, false);
    }


    if (m_server->num_non_init() > 0) {
        //file = fopen("testfile.mp4", iwb");

        if (opts.opt_mux == TS) {
            ts_box(true);
            m_server->broadcast_message<unsigned char>(sendChunks, sendChunks.size(), false, binary, true);
        } else if (opts.opt_mux == MP4 && iFrameCount > 0) {

            m_server->broadcast_message<unsigned char>(headerBuffer, headerBuffer.size(), false, binary, true);
            m_server->broadcast_message<unsigned char>(sendChunks, sendChunks.size(), false, binary, false, setPts);

            //if (file) {
                //fwrite(headerBuffer.data(), 1, headerBuffer.size(), file);
                //fwrite(sendChunks.data(), 1, sendChunks.size(), file);
                //fclose(file);
                //file = NULL;
            //}
        } else {
            m_server->broadcast_message(frame_vector, frame_vector.size(), false, binary, false);
        }

    }


    frame_vector.clear();
    sendChunks.clear();

    if (opts.opt_mux == NONE && opts.opt_seq_num) {
        frame_vector.insert(frame_vector.end(), packet_num);
    }
    packet_num++;

    numFrames = 0;
    iFrameCount = 0;

    frame_size_array.clear();

    return true;
}

void WebSocketSink::afterGettingFrame(void* clientData, unsigned frameSize, unsigned numTruncatedBytes,
        struct timeval presentationTime, unsigned durationInMicroseconds) {
    WebSocketSink* sink = (WebSocketSink*)clientData;
    sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
}

// If you don't want to see debugging output for each received frame, then comment out the following line:

void WebSocketSink::afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
        struct timeval presentationTime, unsigned /*durationInMicroseconds*/) {
    // We've just received a frame of data.  (Optionally) print out information about it:

    if (opts.opt_debug >= DBG_LEVEL_2) {
        if (fStreamId != NULL) {
            envir() << "Stream \"" << fStreamId << "\"; "; envir() << fSubsession.mediumName() << "/" << fSubsession.codecName() << ":\tReceived " << frameSize << " bytes";
        }
        if (numTruncatedBytes > 0) {
            envir() << " (with " << numTruncatedBytes << " bytes truncated)";
        }

        char uSecsStr[6+1]; // used to output the 'microseconds' part of the presentation time
        sprintf(uSecsStr, "%06u", (unsigned)presentationTime.tv_usec);
        envir() << ".\tPresentation time: " << (int)presentationTime.tv_sec << "." << uSecsStr;
        if (fSubsession.rtpSource() != NULL && !fSubsession.rtpSource()->hasBeenSynchronizedUsingRTCP()) {
            envir() << "!"; // mark the debugging output to indicate that this presentation time is not RTCP-synchronized
        }
        envir() << "\tNPT: " << fSubsession.getNormalPlayTime(presentationTime);
        envir() << "\n";
    }

    genChunks(frameSize);
    // Then continue, to request the next frame of data:
    continuePlaying();
}

Boolean WebSocketSink::continuePlaying() {
    if (fSource == NULL) return False; // sanity check (should not happen)

    // Request the next frame of data from our input source.  "afterGettingFrame()" will get called later, when it arrives:
    fSource->getNextFrame(fReceiveBuffer, opts.opt_buffsize,
            afterGettingFrame, this,
            onSourceClosure, this);
    return True;
}
