
#include <iostream> 
#include <string>

#include "BroadcastServer.hh"
#include "Util.hh"
#include "WebSocketRTSPClient.hh"

using websocketpp::lib::placeholders::_1;
using websocketpp::endpoint;

const string BroadcastServer::VIDEO_SETUP = "video_setup";
const string BroadcastServer::PORT_HEADER = "port";

template void BroadcastServer::broadcast_message(vector<unsigned char>, size_t, bool, con_list, websocketpp::frame::opcode::value, void (*)(vector<unsigned char> &, connection_info *&));
template void BroadcastServer::broadcast_message(vector<unsigned char>, size_t, bool, websocketpp::frame::opcode::value, bool, void (*)(vector<unsigned char> &, connection_info *&));
template void BroadcastServer::broadcast_message(unsigned char const *, size_t, bool, websocketpp::frame::opcode::value, bool, void (*)(vector<unsigned char> &, connection_info *&));

BroadcastServer::BroadcastServer() {
    m_control_server.init_asio();
    m_server.init_asio();

    m_control_server.set_open_handler(bind(&BroadcastServer::on_control_open,this,::_1));
    m_control_server.set_close_handler(bind(&BroadcastServer::on_control_close,this,::_1));
    // TODO this callback can handle any messages coming from clients (not implemented)
    //m_control_server.set_message_handler(bind(&WebSocketSink::on_message,this,::_1,::_2));

    m_server.set_open_handler(bind(&BroadcastServer::on_open,this,::_1));
    m_server.set_close_handler(bind(&BroadcastServer::on_close,this,::_1));

    
    m_control_server.clear_access_channels(websocketpp::log::alevel::all);
    m_server.clear_access_channels(websocketpp::log::alevel::all);

    m_control_server.set_reuse_addr(true);
    m_server.set_reuse_addr(true);
}


BroadcastServer::~BroadcastServer() {
    this->stop();
}

void BroadcastServer::close_all() {
    for (auto it: non_init_cons) {
        m_server.close(it.first, websocketpp::close::status::normal, CLOSE_MSG);
    }
    for (auto it: m_connections) {
        m_server.close(it.first, websocketpp::close::status::normal, CLOSE_MSG);
    }

    for (auto it: m_control_connections) {
        m_control_server.close(it.first, websocketpp::close::status::normal, CLOSE_MSG);
    }

}

void BroadcastServer::run(unsigned port) {
        websocketpp::lib::asio::error_code ec = websocketpp::lib::asio::error_code();

    controlServerThread = thread(&BroadcastServer::run_server, this, port, true);

    video_port_mutex.lock();
    video_port = 0;
    serverThread = thread(&BroadcastServer::run_server, this, video_port, false);

    video_port_mutex.lock();
            video_port = m_server.get_local_endpoint(ec).port();

    video_port_to_json();
    video_port_mutex.unlock();
}

void BroadcastServer::stop() {
    close_all();
    m_control_server.stop();
    m_server.stop();
    this->controlServerThread.join();
    this->serverThread.join();
}

void BroadcastServer::run_server(unsigned port, bool control)
{

    if (control) {
        m_control_server.listen(port);
        m_control_server.start_accept();
        std::cout << "[websocket (control)] Listening on ws://localhost:" << port << "/" << std::endl;
        m_control_server.run();
    } else {
        
        m_server.listen(port);

        video_port_mutex.unlock();

        m_server.start_accept();
        std::cout << "[websocket] Listening on ws://localhost/" << video_port << "/" << std::endl;
        m_server.run();
    }
}

void BroadcastServer::video_port_to_json() {
    video_port_message = string();
    util::fill_json_str(video_port_message,
            { make_pair(StreamInfo::MESSAGE_TYPE_HEADER, VIDEO_SETUP)}, 
            { make_pair(PORT_HEADER, this->video_port)});
}

template <typename T>
void BroadcastServer::broadcast_message(vector<T> payload, size_t len, bool control, con_list connection_list, websocketpp::frame::opcode::value op, void (*preprocess)(vector<T> &, connection_info *&))
{
    for (auto it = connection_list.begin(); it != connection_list.end();) {
        if (preprocess) {
            preprocess(payload, it->second);
        }
        if (!(this->send(it->first, control, payload.data(), len, op))) {
            it = connection_list.erase(it);
        } else {
            ++it;
        }
    }

}

template <typename T>
void BroadcastServer::broadcast_message(vector<T> payload, size_t len, bool control, websocketpp::frame::opcode::value op, bool initial, void (*preprocess)(vector<T> &, connection_info *&)) {

    if (control) {
        control_con_list_mutex.lock();
        this->broadcast_message(payload, len, control, m_control_connections, op, preprocess);
        control_con_list_mutex.unlock();
    } else {
        con_list_mutex.lock();
        if (initial) {
            this->broadcast_message(payload, len, control, non_init_cons, op, preprocess);
            for (auto it: non_init_cons) {
                m_connections.insert(it);
            }
            non_init_cons.clear();
        } else {
            this->broadcast_message(payload, len, control, m_connections, op, preprocess);
        }
        con_list_mutex.unlock();
    }
}

template <typename T, typename PType>
void BroadcastServer::broadcast_message(PType const *payload, size_t len, bool control, websocketpp::frame::opcode::value op, bool initial, void (*preprocess)(vector<T> &, connection_info *&)) 
{
    if (payload && payload + len) {
        vector<u_int8_t> data = vector<u_int8_t>(payload, payload + len);
        this->broadcast_message(data, data.size(), control, op, initial, preprocess);
    }
}


bool BroadcastServer::send(connection_hdl hdl, bool control, void const *payload, size_t len, websocketpp::frame::opcode::value op) {
    try {
        if (control) {
            m_control_server.send(hdl, payload, len, op);
        } else {
            m_server.send(hdl, payload, len, op);
        }
    } catch (const websocketpp::exception &e) {
        cerr << e.what();
        return false;
    }

    return true;
}

void BroadcastServer::on_control_open(connection_hdl hdl) 
{
    cout << "[websocket (control)] client connected from " << m_server.get_con_from_hdl(hdl)->get_remote_endpoint() << endl;
    control_con_list_mutex.lock();
    m_control_connections.insert(pair<connection_hdl, connection_info *>(hdl, new connection_info(0, this->pts_incr)));

    try {
        m_control_server.send(hdl, this->video_port_message, websocketpp::frame::opcode::text);
        m_control_server.send(hdl, this->stream_info, websocketpp::frame::opcode::text);
    } catch (const websocketpp::exception &e) {
        cerr << e.what();
    }
    control_con_list_mutex.unlock();
}

void BroadcastServer::on_open(connection_hdl hdl) {
    cout << "[websocket] client connected from " << m_server.get_con_from_hdl(hdl)->get_remote_endpoint() << endl;
    con_list_mutex.lock();

    non_init_cons.insert(pair<connection_hdl, connection_info *>(hdl, new connection_info(0, this->pts_incr)));
    con_list_mutex.unlock();
}

void BroadcastServer::on_control_close(connection_hdl hdl) 
{
    map<connection_hdl, connection_info *>::iterator it;

    control_con_list_mutex.lock();
    it = m_control_connections.find(hdl);

    if (it != m_control_connections.end()) {
        delete it->second;
        m_control_connections.erase(it);
    }

    control_con_list_mutex.unlock();
}

void BroadcastServer::on_close(connection_hdl hdl) {
    map<connection_hdl, connection_info *>::iterator it;

    con_list_mutex.lock();
    it = non_init_cons.find(hdl);

    if (it != non_init_cons.end()) {
        delete it->second;
        non_init_cons.erase(it);
    } else {
        it = m_connections.find(hdl);

        if (it != m_connections.end()) {
            delete it->second;
            m_connections.erase(it);
        }
    }

    con_list_mutex.unlock();
}

int BroadcastServer::num_non_init() {
    return non_init_cons.size();
}
