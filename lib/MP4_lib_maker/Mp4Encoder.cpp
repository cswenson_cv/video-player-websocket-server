/*******************************************************************************
 * Copyright 2007 Cernium Corporation.  All Rights Reserved.
 *******************************************************************************
 *
 * Mp4Encoder.c
 *
 * This file contains functions relating to MP4 file format creation.
 * Version 1.2: Mp4 container for H.264 BP.
 *              Supports variable I frame interval.
 *              Supports P frames of zero size, but not I frames.
 * Version 2.0: Added support for Audio ( AAC )
 *
 ******************************************************************************/

/**************************
 * Standard Linux headers *
 **************************/
#include "Mp4Encoder.hh"
#include <algorithm>
#include <iostream>

using namespace std;

/*******************
* Defines Symbols *
*******************/
#define NUMBER(n)				\
( ((n & 0xFF) << 24) |			\
  ((n & 0xFF00) << 8) |			\
  ((n & 0xFF0000) >> 8) |		\
  ((n & 0xFF000000) >> 24) )
#define TYPE(a, b, c, d) ((a) | (b << 8) | (c << 16) | (d << 24))
#define WRITE_4BYTES(p, counter, num)			\
{												\
	p[counter++] = (num & 0xFF000000) >> 24;	\
	p[counter++] = (num & 0x00FF0000) >> 16;	\
	p[counter++] = (num & 0xFF00) >> 8;			\
	p[counter++] = (num & 0xFF);				\
}
#define WRITE_TYPE(p, counter, a, b, c, d)	\
{						\
	p[counter++] = a;	\
	p[counter++] = b;	\
	p[counter++] = c;	\
	p[counter++] = d;	\
}


// For iso5 compatibility change FILE_TYPE_SIZE to 28, and MP4HEADERSIZE to 36 and add "iso5" in the ftyp header
#define FILE_TYPE_SIZE 24
#define MOVIE_HEADER_SIZE 108
#define TRACK_HEADER_SIZE 92
#define MEDIA_HEADER_SIZE 32
#define HANDLER_REFERENCE_SIZE 36	//32+4
#define VIDEO_MEDIA_INFO_HEADER_SIZE 20
#define SOUND_MEDIA_INFO_HEADER_SIZE 16
#define DATA_REFERENCE_SIZE 28
#define DATA_INFO_SIZE (8+DATA_REFERENCE_SIZE)
#define PPS_SIZE 4

#define SAMPLE_SIZE_SIZE (20+4*num_entries)
#define SAMPLE_TO_CHUNK_SIZE (28)

#define MAX_TIME_TABLE_ENTRIES 20
#define SAMPLING_FREQ_TABLE_SIZE 12

unsigned pts_video;
unsigned pts_audio;

 /************************
 * Function Definitions *
 ************************/
 /******************************************************************************
*
* Function      :       generateTime2Sample
*
* Description   :       This function generates the time-to-sample table and
* 						calculates the total number of time-to-sample entries.
*						The max. number of entries is totalNumFrm/2 but this
*						function caps the max. number of entries to
*						MAX_TIME_TABLE_ENTRIES.
*
* Parameters    :       [IN] totalNumFrm - total number of frames in the video
* 						[OUT] *table - array to store the table values
*
* Return Value  :       Return number of time-to-sample entries on success and
*						-1 on failure
*
*******************************************************************************/
static int generateTime2Sample( unsigned int *table, int totalNumFrm,
								const frameInfo *frameSizeArray, int fps )
{
	int i, index = 0; // num_timeEntries
	int sampleCount = 0;
	int consecutiveZeroFrames = 0;

    if (totalNumFrm == 0) {
        return 0;
    }

	// Assume frameSizeArray[0].frameSize != 0
	for( i = 0; i < totalNumFrm; i++ )
	{
		if( frameSizeArray[i].frameSize != 0 )
		{
			sampleCount++;

			// Write out previously accumlated zero frame sample count
			if( consecutiveZeroFrames )
			{
				table[index] = 1;	// sample count
				table[index+1] = (TIME_SCALE/fps) *
								 (consecutiveZeroFrames+1);	// sample duration. +1 to include the frame before the zero length frame
				index+=2;
				consecutiveZeroFrames = 0;
			}
		}
		else
		{
			// Write out previously accumlated non-zero frame sample count
			if( consecutiveZeroFrames == 0 )
			{
				if( sampleCount > 1 )
				{
					table[index] = sampleCount-1;		// sample count. -1 to exclude the frame before the zero length frame
					table[index+1] = TIME_SCALE/fps;	// sample duration
					index+=2;
				}
			}

			sampleCount = 0;
			consecutiveZeroFrames++;
		}

		// Cap the max. number of entries in the time table
		if( index >= (MAX_TIME_TABLE_ENTRIES-2)*2 )
			break;
	}

	// If end in zero size frames
	if( consecutiveZeroFrames )
	{
		table[index] = 1;
		table[index+1] = (TIME_SCALE/fps) *
						 (consecutiveZeroFrames+1);
		index+=2;
	}
	else
	{
		table[index] = sampleCount;
		table[index+1] = TIME_SCALE/fps;
		index+=2;
	}

	// If the table is capped, enter the last entry
	if( (i+1) < totalNumFrm )
	{
		table[index] = totalNumFrm - 1 - i;
		table[index+1] = TIME_SCALE/fps;
		index+=2;
	}

//	for( i = 0; i < index; i+=2 )
//		printf("[%d]: %d\t%d\n", i, table[i], table[i+1]);

	return index/2;
}

 /******************************************************************************
*
* Function      :       generateChunkOffsetTable
*
* Description   :       This function generates the chunk offset table and
* 						calculates the total encoded bitstream + 8.
*
* Parameters    :       [IN] totalNumFrm - total number of frames in the video
* 						[OUT] *table - array to store the table values
*
* Return Value  :       0 on success and -1 on failure
*
*******************************************************************************/
/*
static int generateChunkOffsetTable( unsigned int *table, int *mdatSize,
									 int totalNumFrm, const frameInfo *frameSizeArray,
									 int moovSize)
{
	int i, index;

	if( table == NULL)
		return MP4_ERROR;

	if( moovSize )
		table[0] = MP4HEADERSIZE + moovSize;
	else
		table[0] = MP4HEADERSIZE;	// if moov at footer, then first entry is always this size (size of MP4 header)

	for( i = 0, index = 0; i < totalNumFrm; i++ )
	{
		if( frameSizeArray[i].frameSize != 0 )
		{
			table[index+1] = table[index] + frameSizeArray[i].frameSize;
			index++;
		}

		*mdatSize += frameSizeArray[i].frameSize;
	}

	printf("mdatSize: %d\n", *mdatSize);
	for( i = 0; i < index+1; i++ )
		printf("table[%d]: %d\n", i, table[i]);

	return MP4_OK;
}
*/	// Will be provided as part of the call
 /******************************************************************************
*
* Function      :       generateIFramesTable
*
* Description   :       This function generates the I-frames table so that it
*						can support variable I-frame interval.
*
* Parameters    :       [IN] totalNumFrm - total number of frames in the video
* 						[OUT] *table - array to store the table values
*
* Return Value  :       Return total number of I-frames on success and
*						-1 on failure
*
*******************************************************************************/
static int generateIFramesTable( unsigned int *table, int totalNumFrm,
				 const frameInfo *frameSizeArray, int maxFrames )
{
	int i, index, num_IFrames = 0;

	for( i = 0, index = 0; i < totalNumFrm; i++ )
	{
		if( frameSizeArray[i].IFrame == 1 )
		{
			table[index] = i+1; // index starts from 1
			index++;
			num_IFrames++;
		}
		// This check prevents us from writing more Iframes than expected
		if (maxFrames && (num_IFrames == maxFrames))
			break;
	}

	return num_IFrames;
}

 /******************************************************************************
*
* Function      :       getSPSSize
*
* Description   :       This function search for the size of SPS and update
* 						other variables that is dependent on it.
*
* Parameters    :       [IN] *H264_Header - Header buffer
* 						[OUT] *spsSize - size of SPS
*
* Return Value  :       0 on success and -1 on failure
*
*******************************************************************************/
static int getSPSSize( char *H264_Header, int H264HeaderSize, int *stsdSize,
					   int *stsdTableSize, int *avcCSize,
					   int *spsSize, int *ppsSize )
{
	int i, ppsStart;

	if( (H264_Header == NULL) || (stsdSize == NULL) || (stsdTableSize == NULL) ||
		(avcCSize == NULL) || (spsSize == NULL) || (ppsSize == NULL) )
	return MP4_ERROR;


	// Search for the size of SPS
	for( i = 8; i < H264HeaderSize - 4; i++ )
	{
		if( (H264_Header[i] == 0) && (H264_Header[i+1] == 0) &&
			(H264_Header[i+2] == 0) && (H264_Header[i+3] == 1) )
		{
			*spsSize = i - 4;
			break;
		}
	}
	i+=4;
	*ppsSize = 4; // pps size is normally 4

	if ((i < H264HeaderSize) && ((H264_Header[i] & 0x1f) == 0x8))
	{
		ppsStart = i;
		for(; i < H264HeaderSize - 4; i++ )
		{
			if( (H264_Header[i] == 0) && (H264_Header[i+1] == 0) &&
				(H264_Header[i+2] == 0) && (H264_Header[i+3] == 1) )
			{
				*ppsSize = i - ppsStart;
				break;
			}
		}
		if(i==(H264HeaderSize - 4))
			*ppsSize = H264HeaderSize - ppsStart;
	}

	*avcCSize = 19 + *spsSize + *ppsSize;
	*stsdTableSize = 84 + 2 + *avcCSize;
	*stsdSize = 16 + *stsdTableSize;

	return MP4_OK;
}

/******************************************************************************
*
* Function      :       getH264Header
*
* Description   :       This function extracts the H.264 header and search for
*						the size of the header.
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*
* Return Value  :       Returns number of bytes of the MP4 header on success
* 						and -1 on failure.
*
*******************************************************************************/
int getH264Header( const unsigned char *buffer, vector<char> &H264Header, int H264HeaderBufSize)
{
    int H264HeaderSize;
	int i, spsStart=0, spsFound=0, spsSize=0;
	int ppsStart=0, ppsSize=0;

	if( (buffer == NULL))
		return MP4_ERROR;

	// Search for the SPS
	for( i = 0; i < H264HeaderBufSize - 5; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) && (buffer[i+4] & 0x1F) == 0x7)
		{
                        //printf("sps is at %d\n", i);
			spsStart=i;
			spsFound=1;
			break;
		}
	}

	if (!spsFound)
		return MP4_ERROR;

	// Search for the size of SPS
	for( i = (spsStart+4); i < H264HeaderBufSize - 4; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) )
		{
                        //printf("value of i is %d\n", i);
			spsSize = i - spsStart;
			break;
		}
	}
	if (spsSize == 0) // Reached end of frame
		spsSize = H264HeaderBufSize - spsStart;

	// Search for the PPS. PPS always follows SPS
	if ((i < (H264HeaderBufSize - 5)) && 
	    ((buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
	     && (buffer[i+3] == 1) && ((buffer[i+4] & 0x1F) == 0x8)))
		ppsStart=i;
	else
		return MP4_ERROR;
	
	// Search for the size of PPS
	for( i = (ppsStart+4); i < H264HeaderBufSize - 4; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) )
		{
                        //printf("value of i is %d\n", i);
			ppsSize = i - ppsStart;
			break;
		}
	}

	if (ppsSize == 0) // Reached end of frame
		ppsSize = H264HeaderBufSize - ppsStart;

	H264HeaderSize = spsSize + ppsSize;
	if (H264HeaderSize > H264HeaderBufSize)
		return MP4_ERROR;

	// save H264_Header, will be insert to footer later
	//memcpy( H264Header, buffer+spsStart, *H264HeaderSize);
    H264Header.insert(H264Header.end(), buffer + spsStart, buffer + spsStart + H264HeaderSize);

/*	printf( "H264_Header:\n" );
	for( i = 0; i < *H264HeaderSize; i++ )
		printf( "%x ", H264Header[i] );
	printf("\n");
*/

	return MP4_OK;
}
/******************************************************************************
*
* Function      :       getH264Header
*
* Description   :       This function extracts the H.264 header and search for
*						the size of the header.
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*
* Return Value  :       Returns number of bytes of the MP4 header on success
* 						and -1 on failure.
*
*******************************************************************************/
int getH264Header( const char *buffer, char *H264Header, int H264HeaderBufSize,
				   int *H264HeaderSize )
{
	int i, spsStart=0, spsFound=0, spsSize=0;
	int ppsStart=0, ppsSize=0;

	if( (buffer == NULL) || (H264Header == NULL) || (H264HeaderSize == NULL) )
		return MP4_ERROR;

	// Search for the SPS
	for( i = 0; i < H264HeaderBufSize - 5; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) && (buffer[i+4] & 0x1F) == 0x7)
		{
                        //printf("sps is at %d\n", i);
			spsStart=i;
			spsFound=1;
			break;
		}
	}

	if (!spsFound)
		return MP4_ERROR;

	// Search for the size of SPS
	for( i = (spsStart+4); i < H264HeaderBufSize - 4; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) )
		{
                        //printf("value of i is %d\n", i);
			spsSize = i - spsStart;
			break;
		}
	}
	if (spsSize == 0) // Reached end of frame
		spsSize = H264HeaderBufSize - spsStart;

	// Search for the PPS. PPS always follows SPS
	if ((i < (H264HeaderBufSize - 5)) && 
	    ((buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
	     && (buffer[i+3] == 1) && ((buffer[i+4] & 0x1F) == 0x8)))
		ppsStart=i;
	else
		return MP4_ERROR;
	
	// Search for the size of PPS
	for( i = (ppsStart+4); i < H264HeaderBufSize - 4; i++ )
	{
		if( (buffer[i] == 0) && (buffer[i+1] == 0) && (buffer[i+2] == 0)
		 && (buffer[i+3] == 1) )
		{
                        //printf("value of i is %d\n", i);
			ppsSize = i - ppsStart;
			break;
		}
	}

	if (ppsSize == 0) // Reached end of frame
		ppsSize = H264HeaderBufSize - ppsStart;

	*H264HeaderSize = spsSize + ppsSize;
	if (*H264HeaderSize > H264HeaderBufSize)
		return MP4_ERROR;

	// save H264_Header, will be insert to footer later
	memcpy( H264Header, buffer+spsStart, *H264HeaderSize);

/*	printf( "H264_Header:\n" );
	for( i = 0; i < *H264HeaderSize; i++ )
		printf( "%x ", H264Header[i] );
	printf("\n");
*/

	return MP4_OK;
}

/******************************************************************************
*
* Function      :       generateMp4Header_IDR_Frame
*
* Description   :       This function generates the 32 bytes header, except the
*						size of 'mdat'.
*
* Parameters    :       [IN] *buffer - Buffer to store the MP4 header
*
* Return Value  :       Returns number of bytes of the MP4 header on success
* 						and -1 on failure.
*
*******************************************************************************/
int generateMp4Header_IDR_Frame( char *buffer, int bufferSize )
{
	int bytesCount = 0;

	if( (buffer == NULL) || (bufferSize != MP4HEADERSIZE) )
		return MP4_ERROR;

	WRITE_4BYTES(buffer, bytesCount, FILE_TYPE_SIZE)
	WRITE_TYPE(buffer, bytesCount, 'f','t','y','p')
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_4BYTES(buffer, bytesCount, 0)
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_TYPE(buffer, bytesCount, 'i','s','o','m')
    //WRITE_TYPE(buffer, bytesCount, 'i','s','o','5')
	WRITE_4BYTES(buffer, bytesCount, 0)	// will insert mdatSize at footer
	WRITE_TYPE(buffer, bytesCount, 'm','d','a','t')

    return MP4HEADERSIZE;
}

/******************************************************************************
*
* Function      :       generateMp4Header_I_Frame
*
* Description   :       This function generates the 32 bytes header, except the
*						size of 'mdat'.
*
* Parameters    :       [IN] *buffer - Buffer to store the MP4 header
*
* Return Value  :       Returns number of bytes of the MP4 header on success
* 						and -1 on failure.
*
*******************************************************************************/
int generateMp4Header_I_Frame( char *buffer, int bufferSize )
{
	// Same code as generateMp4Header_IDR_Frame(). Keep this for code compatibility
	return generateMp4Header_IDR_Frame( buffer, bufferSize );
}

/******************************************************************************
*
* Function      :       writeVideoStsd
*
* Description   :       This function writes the video sample description atom
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*						[IN] footerSizeBytes - Bytes already written to buffer
*						[OUT] stsdSize  - size of the stsd atom
*
* Return Value  :       Returns total number of bytes written on success
* 						and -1 on failure.
*
*******************************************************************************/
static int writeVideoStsd(char *buffer, int footerSizeBytes, VideoInfo *videoInfo
						, char *errorString, int *stsdSize)
{
	int stsdTableSize=0, avcCSize=0, spsSize=0, ppsSize=0, value, i;

	if( MP4_ERROR == getSPSSize(videoInfo->H264_Header, videoInfo->H264HeaderSize, stsdSize,
								&stsdTableSize, &avcCSize,
								&spsSize, &ppsSize) )
	{
		sprintf( errorString, "Improper H264_Header information!\n" );
		return MP4_ERROR;
	}
	value = *stsdSize;		// We do this since WRITE_4BYTES needs an int, not a pointer
	WRITE_4BYTES(buffer, footerSizeBytes, value)
	WRITE_TYPE(buffer, footerSizeBytes, 's','t','s','d')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 1)
	WRITE_4BYTES(buffer, footerSizeBytes, stsdTableSize)
	WRITE_TYPE(buffer, footerSizeBytes, 'a','v','c','1')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 1)		// data reference index
	// Video sample description
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)		// vendor
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, ((videoInfo->width << 16) | videoInfo->height) )// width & height
	WRITE_4BYTES(buffer, footerSizeBytes, 72 << 16)		// 72 dpi
	WRITE_4BYTES(buffer, footerSizeBytes, 72 << 16)		// 72 dpi
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 1 << 16)		// frame count
	for( i = 0; i < 28; i++ )					// compressor name (2+28+2 bytes)
		buffer[footerSizeBytes++] = 0;
	WRITE_4BYTES(buffer, footerSizeBytes, 24)			// depth

	buffer[footerSizeBytes++] = 0xFF;		// default color table
	buffer[footerSizeBytes++] = 0xFF;		// default color table

	// Video sample description extensions
	buffer[footerSizeBytes++] = (avcCSize & 0xFF000000) >> 24;
	buffer[footerSizeBytes++] = (avcCSize & 0x00FF0000) >> 16;
	buffer[footerSizeBytes++] = (avcCSize & 0xFF00) >> 8;
	buffer[footerSizeBytes++] = (avcCSize & 0xFF);
	WRITE_TYPE(buffer, footerSizeBytes, 'a','v','c','C')
	// Magic numbers!
	buffer[footerSizeBytes++] = 0x01;
	buffer[footerSizeBytes++] = videoInfo->H264_Header[5];	//profile_idc
	buffer[footerSizeBytes++] = videoInfo->H264_Header[6];	//constraint flags
	buffer[footerSizeBytes++] = videoInfo->H264_Header[7];	//level_idc
	buffer[footerSizeBytes++] = 0xFF;
	buffer[footerSizeBytes++] = 0xE1;
	buffer[footerSizeBytes++] = 0x00;
	buffer[footerSizeBytes++] = spsSize;

	memcpy(buffer + footerSizeBytes, videoInfo->H264_Header + 4, spsSize);	// SPS
	footerSizeBytes += spsSize;		// updates the number of bytes written so far

	// Magic numbers!
	buffer[footerSizeBytes++] = 0x01;
	buffer[footerSizeBytes++] = 0x00;
	buffer[footerSizeBytes++] = ppsSize;

	memcpy(buffer + footerSizeBytes, videoInfo->H264_Header + 4 + spsSize + 4, ppsSize);	// PPS
	footerSizeBytes += ppsSize;		// updates the number of bytes written so far

    return footerSizeBytes;
}

/******************************************************************************
*
* Function      :       writeAudioStsd
*
* Description   :       This function writes the audio sample description atom
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*						[IN] footerSizeBytes - Bytes already written to buffer
*						[OUT] stsdSize  - size of the stsd atom
*
* Return Value  :       Returns total number of bytes written on success
* 						and -1 on failure.
*
*******************************************************************************/
static int writeAudioStsd(char *buffer, int footerSizeBytes, AudioInfo *audioInfo
						, char *errorString, int *stsdSize)
{
	int specificInfoLength, esdsSize, stsdTableSize, value, i;
	int objectProfile, samplingFreqIndex = 0;
	int samplingFreqTable[SAMPLING_FREQ_TABLE_SIZE ] = 
							{96000, 88200, 64000, 48000, 44100, 32000,
							 24000, 22050, 16000, 12000, 11025, 8000 };

	specificInfoLength = 2;
	esdsSize = 49 + specificInfoLength;
	stsdTableSize = esdsSize + 9 * 4;
	*stsdSize = stsdTableSize + 4 * 4;

	value = *stsdSize;
	WRITE_4BYTES(buffer, footerSizeBytes, value)
	WRITE_TYPE(buffer, footerSizeBytes, 's','t','s','d')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)	// 1 byte version, 3 bytes of flags
	WRITE_4BYTES(buffer, footerSizeBytes, 1)	// number of entries
	WRITE_4BYTES(buffer, footerSizeBytes, stsdTableSize)
	WRITE_TYPE(buffer, footerSizeBytes, 'm','p','4','a')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)	// six bytes that must be 0 ( reserved )
	WRITE_4BYTES(buffer, footerSizeBytes, 1)	// data reference index( the last 2 bytes of the 4 here)

	WRITE_4BYTES(buffer, footerSizeBytes, 0)	// 2 bytes of version = 0, 2 of revision - has to be 0
	WRITE_4BYTES(buffer, footerSizeBytes, 0)	// 4 bytes of vendor - has to be 0

	// 2 bytes of noOfChannels and 2 bytes of bitsPerSample
	WRITE_4BYTES(buffer, footerSizeBytes, ( (audioInfo->noOfChannels << 16 ) | audioInfo->bitsPerSample))	

	// packet size =0 and first 2 bytes of timescale/sampleRate
	WRITE_4BYTES(buffer, footerSizeBytes, ( audioInfo->sampleRate >> 16 ))	

	// last 2 bytes of timescale/sampleRate and 2 bytes resvd = 0
	WRITE_4BYTES(buffer, footerSizeBytes, ( audioInfo->sampleRate << 16 ))	

	WRITE_4BYTES(buffer, footerSizeBytes, esdsSize)
	WRITE_TYPE(buffer, footerSizeBytes, 'e','s','d','s')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)	// 1 byte version, 3 bytes of flags

	// es descriptor
	buffer[footerSizeBytes++] = 0x03;	// Tag
	WRITE_4BYTES(buffer, footerSizeBytes, 0x80808022)	// 0x22 is the length
	buffer[footerSizeBytes++] = 0;		// First 8 bits of ESID
	buffer[footerSizeBytes++] = 0;		// Next 8 bits of ESID
	buffer[footerSizeBytes++] = 0;		// 1 bit - streamdependence flag, 1 bit - url flag, 
										// 1 bit resvd. = 1, 5 bits - stream priority

	// decoder config descriptor - part of es descr
	buffer[footerSizeBytes++] = 0x04;	// Tag
	WRITE_4BYTES(buffer, footerSizeBytes, 0x80808014)	// 0x14 is the length
	buffer[footerSizeBytes++] = 0x40;   // objectTypeId = AAC Main, 
	buffer[footerSizeBytes++] = 0x15;   // first 6 bits are streamtype, next bit upStream=0, 
										// next bit-reserved = 1, streamtype5-audiostream
	int bufferSizeDB = 0;
	buffer[footerSizeBytes++] = bufferSizeDB >> 16 ;	// bufferSizeDB
	buffer[footerSizeBytes++] = bufferSizeDB >> 8 ;		// bufferSizeDB
	buffer[footerSizeBytes++] = bufferSizeDB & 0xff ;	// bufferSizeDB
	WRITE_4BYTES(buffer, footerSizeBytes, audioInfo->bitRate)	// maxBitRate
	WRITE_4BYTES(buffer, footerSizeBytes, audioInfo->bitRate)	// avgBitRate

	// decoder specific info - part of decoder config descr
	buffer[footerSizeBytes++] = 0x05;	// Tag
	WRITE_4BYTES(buffer, footerSizeBytes, (0x80808000 | specificInfoLength))	// length

	objectProfile = ( audioInfo->aacLC == 1 ) ? 2 : 1;	// Profile is 2 for LC, 1 for HE
 
	samplingFreqIndex = -1;
	for(i=0;i<SAMPLING_FREQ_TABLE_SIZE;i++)
		if( audioInfo->sampleRate == samplingFreqTable[i] )
			samplingFreqIndex = i;
	if(samplingFreqIndex == -1 )
	{
		sprintf( errorString, "Invalid frequency %d\n", audioInfo->sampleRate);
		return MP4_ERROR;
	}

	// 5 bits of objectProfile, 4 bits of samplingFreqIndex, 4 bits of Channel, 3 bits of 0
	buffer[footerSizeBytes++] = (char ) ( (objectProfile << 3) & 0xf8 ) | (( samplingFreqIndex >> 1 ) & 0x7) ;
	buffer[footerSizeBytes++] = (char ) ((samplingFreqIndex << 7) & 0x80 ) |  ( (audioInfo->noOfChannels << 3) & 0x78 );

	// SL config descriptor - part of es descr
	buffer[footerSizeBytes++] = 0x06;					// Tag
	WRITE_4BYTES(buffer, footerSizeBytes, 0x80808001)	// 0x01 is the length
	buffer[footerSizeBytes++] = 0x02;					// predefined = 2 indicates reserved for ISO use

	return footerSizeBytes;
}

/******************************************************************************
*
* Function      :       writeTrak
*
* Description   :       This function writes the audio/video trak
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*						[IN] footerSizeBytes - Bytes already written to buffer
*						[IN] id -  trak id ( assume 1 for video, 2 for audio )
*						[OUT] trakSize  - size of the trak
*
* Return Value  :       Returns total number of bytes written on success
* 						and -1 on failure.
*
*******************************************************************************/
static int writeTrak(char *buffer, int footerSizeBytes,VideoInfo *videoInfo,
			  		 AudioInfo *audioInfo, int id, char *errorString, 
					 unsigned int *scratchTable, int *trakSize)
{
	int tkhdDuration, mdhdDuration, value, totalNumFrm, i, timePerFrame, samplesPerFrame;
	int num_IFrames = 0, num_entries = 0, num_timeEntries = 0;
	int stsdSize, sttsSize, stszSize, stcoSize, stssSize, stblSize, stscSize, minfSize, mdiaSize;
	int trakByteLocation, mdiaByteLocation, minfByteLocation, stblByteLocation, stscByteLocation;
	unsigned int *chunkOffsetTable;
	frameInfo *frameSizeArray;

	// For simplicity, we assume trak id=1 is  always video and id=2 is always audio	
	if(id==1)
	{
		totalNumFrm = videoInfo->totalNumFrm;
		frameSizeArray = videoInfo->frameSizeArray;
		chunkOffsetTable = videoInfo->chunkOffsetTable;
		// Calculates the total number of non-zero frames
		for( i = 0; i < totalNumFrm; i++ )
		{
			if( frameSizeArray[i].frameSize != 0 )
				num_entries++;
		}
		timePerFrame = TIME_SCALE/videoInfo->fps;
		tkhdDuration = timePerFrame*totalNumFrm;
		mdhdDuration = tkhdDuration;
	}
	else
	{
		totalNumFrm = audioInfo->totalNumFrm;
		frameSizeArray = audioInfo->frameSizeArray;
		chunkOffsetTable = audioInfo->chunkOffsetTable;
		num_entries = totalNumFrm;	// We assume there are no zero size frames in audio
		samplesPerFrame = ( audioInfo->aacLC == 1 ) ? 1024 : 2048;
		// The overall timescale is different from the timescale of the audio trak
		// The duration in mdhd is as per the audio timescale
		// The duration in tkhd is as per the overall timescale
		mdhdDuration = totalNumFrm * samplesPerFrame;
		// Typecast the values as double so that the result isnt truncated
		tkhdDuration = ((double)mdhdDuration/(double)audioInfo->sampleRate)*TIME_SCALE;

	}

  	// 'trak'
	// trakSize will be updated at the end
	trakByteLocation = footerSizeBytes;
	footerSizeBytes+=4;
	WRITE_TYPE(buffer, footerSizeBytes, 't','r','a','k')
	// 'tkhd'
	WRITE_4BYTES(buffer, footerSizeBytes, TRACK_HEADER_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 't','k','h','d')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// version, flags
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// creation time
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// modification time
	WRITE_4BYTES(buffer, footerSizeBytes, id)			// track id
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// reserved
	WRITE_4BYTES(buffer, footerSizeBytes, tkhdDuration)	// duration ( in terms of mvhd timescale)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// reserved
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// reserved
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0x01000000)	// 16 bit volume
	WRITE_4BYTES(buffer, footerSizeBytes, 0x00010000)	// matrix[0,0]
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0x00010000)	// matrix[1,1]
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0x40000000)	// matrix[2,2]
	WRITE_4BYTES(buffer, footerSizeBytes, (id==1) ? (videoInfo->width << 16) : 0)	// track width
	WRITE_4BYTES(buffer, footerSizeBytes, (id==1) ? (videoInfo->height << 16) : 0)	// track height

	// 'mdia'
	// mdiakSize will be updated at the end
	mdiaByteLocation = footerSizeBytes;
	footerSizeBytes+=4;
	WRITE_TYPE(buffer, footerSizeBytes, 'm','d','i','a')
	// 'mdhd'
	WRITE_4BYTES(buffer, footerSizeBytes, MEDIA_HEADER_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 'm','d','h','d')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// creation time
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// modification time
	WRITE_4BYTES(buffer, footerSizeBytes, (id==1) ? TIME_SCALE : audioInfo->sampleRate)	// Timescale
	WRITE_4BYTES(buffer, footerSizeBytes, mdhdDuration)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	// 'hdlr'
	WRITE_4BYTES(buffer, footerSizeBytes, HANDLER_REFERENCE_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 'h','d','l','r')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)				// component type
	if(id == 1)
		WRITE_TYPE(buffer, footerSizeBytes, 'v','i','d','e')	// component subtype
	else
		WRITE_TYPE(buffer, footerSizeBytes, 's','o','u','n')	// component subtype
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)				// ??? component name

	// 'minf'
	// minfSize will be updated at the end
	minfByteLocation = footerSizeBytes;
	footerSizeBytes+=4;
	WRITE_TYPE(buffer, footerSizeBytes, 'm','i','n','f')
	// 'mdhd'
	WRITE_4BYTES(buffer, footerSizeBytes, (id==1) ? VIDEO_MEDIA_INFO_HEADER_SIZE : SOUND_MEDIA_INFO_HEADER_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, (id==1) ? 'v' : 's','m','h','d')
	WRITE_4BYTES(buffer, footerSizeBytes, (id==1) ? 1 : 0)	// flag - 0 for sound, 1 for video
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	if(id==1)
		WRITE_4BYTES(buffer, footerSizeBytes, 0)			// 4 extra reserved bytes for video
	// 'dinf'
	WRITE_4BYTES(buffer, footerSizeBytes, DATA_INFO_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 'd','i','n','f')
	// 'dref'
	WRITE_4BYTES(buffer, footerSizeBytes, DATA_REFERENCE_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 'd','r','e','f')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 1)
	WRITE_4BYTES(buffer, footerSizeBytes, 12)		// size of data reference entry
	WRITE_TYPE(buffer, footerSizeBytes, 'u','r','l',' ')
	WRITE_4BYTES(buffer, footerSizeBytes, 1)

	// 'stbl'
	// stblSize will be updated at the end
	stblByteLocation = footerSizeBytes;
	footerSizeBytes+=4;
	WRITE_TYPE(buffer, footerSizeBytes, 's','t','b','l')
	// 'stsd'

	if(id==1)
		footerSizeBytes = writeVideoStsd(buffer, footerSizeBytes, videoInfo, errorString, &stsdSize);
    else
		footerSizeBytes = writeAudioStsd(buffer, footerSizeBytes, audioInfo, errorString, &stsdSize);

	if(footerSizeBytes < 0)
		return MP4_ERROR;

	// 'stts'
	// Generates the time-to-sample table
	if(id==1)
		num_timeEntries = generateTime2Sample( scratchTable, totalNumFrm, frameSizeArray, videoInfo->fps );
	else
		num_timeEntries = 1;	// Assuming there will be no zero sized frames for audio

	sttsSize = 16 + 8*num_timeEntries;

	WRITE_4BYTES(buffer, footerSizeBytes, sttsSize)
	WRITE_TYPE(buffer, footerSizeBytes, 's', 't', 't', 's')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, num_timeEntries)	// number of entries

	if(id==1)
	{
		for( i = 0; i < num_timeEntries*2; i+=2 )
		{
			WRITE_4BYTES(buffer, footerSizeBytes, scratchTable[i])	// sample count
			WRITE_4BYTES(buffer, footerSizeBytes, scratchTable[i+1])// duration per sample
		}
	}
	else
	{
		WRITE_4BYTES(buffer, footerSizeBytes, totalNumFrm)
		WRITE_4BYTES(buffer, footerSizeBytes, ((audioInfo->aacLC==1) ? 1024 : 2048))	// 1024 for LC, 2048 for HE
	}

	// 'stsz'
    stszSize = 20+4*num_entries;
	WRITE_4BYTES(buffer, footerSizeBytes, stszSize)
	WRITE_TYPE(buffer, footerSizeBytes, 's', 't', 's', 'z')
	for( i = 0; i < 8; i++ )
		buffer[footerSizeBytes++] = 0;

	WRITE_4BYTES(buffer, footerSizeBytes, num_entries)		// number of entries
	for( i = 0; i < totalNumFrm; i++ )						// sample size tables
	{
		if( frameSizeArray[i].frameSize != 0 )
			WRITE_4BYTES(buffer, footerSizeBytes, frameSizeArray[i].frameSize)
	}

	// 'stsc'
    stscByteLocation = footerSizeBytes;
    footerSizeBytes += 4;
	//WRITE_4BYTES(buffer, footerSizeBytes, SAMPLE_TO_CHUNK_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 's', 't', 's', 'c')
	for( i = 0; i < 4; i++ )
		buffer[footerSizeBytes++] = 0;

	// Each chunk is a single frame
    if (totalNumFrm == 0) {
        WRITE_4BYTES(buffer, footerSizeBytes, 0)	// number of entries

    } else {
        WRITE_4BYTES(buffer, footerSizeBytes, 1)	// number of entries
        WRITE_4BYTES(buffer, footerSizeBytes, 1)	// chunk number
        WRITE_4BYTES(buffer, footerSizeBytes, 1)	// samples per chunk
        WRITE_4BYTES(buffer, footerSizeBytes, 1)	// sample description ID
    }

    stscSize = footerSizeBytes - stscByteLocation;
    WRITE_4BYTES(buffer, stscByteLocation, stscSize)

	// 'stco'
	stcoSize = 16 + 4 * num_entries;
	WRITE_4BYTES( buffer, footerSizeBytes, stcoSize )
	WRITE_TYPE(buffer, footerSizeBytes, 's', 't', 'c', 'o')
	for( i = 0; i < 4; i++ )
		buffer[footerSizeBytes++] = 0;

	WRITE_4BYTES(buffer, footerSizeBytes, num_entries)	// number of entries

	for( i = 0; i < num_entries; i++ )					// chunk offset tables
		WRITE_4BYTES(buffer, footerSizeBytes, chunkOffsetTable[i])

	// 'stss'
	if(id==1)
	{
		num_IFrames = generateIFramesTable( scratchTable, totalNumFrm, frameSizeArray,
 						    videoInfo->iFrameInt ? totalNumFrm/videoInfo->iFrameInt : 0);

		stssSize = 16 + 4 * num_IFrames;
		WRITE_4BYTES(buffer, footerSizeBytes, stssSize)
		WRITE_TYPE(buffer, footerSizeBytes, 's', 't', 's', 's')
		for( i = 0; i < 4; i++ )
			buffer[footerSizeBytes++] = 0;

		WRITE_4BYTES(buffer, footerSizeBytes, num_IFrames)	// number of entries
		for( i = 0; i < num_IFrames; i++ )					// sync sample table
			WRITE_4BYTES( buffer, footerSizeBytes, scratchTable[i] )
	}
	else
		stssSize = 0;		// No stss for audio

	// Now, we go back and update the size of the box containers, in reverse order
	// stsd, stts, stsz, stsc, stco, stss
	stblSize = 8 + stsdSize + sttsSize + stszSize + stscSize + stcoSize + stssSize;
	minfSize = 8 + DATA_INFO_SIZE + stblSize;
	minfSize += (id==1) ? VIDEO_MEDIA_INFO_HEADER_SIZE : SOUND_MEDIA_INFO_HEADER_SIZE;
	mdiaSize = 8 + MEDIA_HEADER_SIZE + HANDLER_REFERENCE_SIZE + minfSize;
	*trakSize = 8 + TRACK_HEADER_SIZE + mdiaSize;

	WRITE_4BYTES( buffer, stblByteLocation, stblSize )
	WRITE_4BYTES( buffer, minfByteLocation, minfSize )
	WRITE_4BYTES( buffer, mdiaByteLocation, mdiaSize )
	value = *trakSize;
	WRITE_4BYTES( buffer, trakByteLocation, value )

	return footerSizeBytes;
}

static int writeMvex(char *buffer, int footerSizeBytes,VideoInfo *videoInfo,
			  		 AudioInfo *audioInfo, char *errorString, 
					 unsigned int *scratchTable, int *trakSize)
{
    int mvexByteLocation = footerSizeBytes, mvexSize;

	footerSizeBytes+=4;
    WRITE_TYPE(buffer, footerSizeBytes, 'm', 'v', 'e', 'x')
    WRITE_4BYTES(buffer, footerSizeBytes, MEHDSIZE)
    WRITE_TYPE(buffer, footerSizeBytes, 'm', 'e', 'h', 'd')
    WRITE_4BYTES(buffer, footerSizeBytes, 0)
    WRITE_4BYTES(buffer, footerSizeBytes, 0)


    if (videoInfo != NULL) {
        // TREXSIZE = 8 (header) + 4 (version, flags) + 4 (track_id) +  4 (desc_index) + 4 (sample_duration) + 4 (sample_size) + 4 (sample_flags)
        WRITE_4BYTES(buffer, footerSizeBytes, TREXSIZE)
        WRITE_TYPE(buffer, footerSizeBytes, 't', 'r', 'e', 'x')
        // Version, flags, track id, defaults
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 1)
        WRITE_4BYTES(buffer, footerSizeBytes, 1)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
    }

    if (audioInfo != NULL) {
        WRITE_4BYTES(buffer, footerSizeBytes, TREXSIZE)
        WRITE_TYPE(buffer, footerSizeBytes, 't', 'r', 'e', 'x')
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 2)
        WRITE_4BYTES(buffer, footerSizeBytes, 1)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
        WRITE_4BYTES(buffer, footerSizeBytes, 0)
    }

    mvexSize = footerSizeBytes - mvexByteLocation;

    *trakSize = mvexSize;

    WRITE_4BYTES(buffer, mvexByteLocation, mvexSize);


    return footerSizeBytes;
}

/******************************************************************************
*
* Function      :       getMdatSize
*
* Description   :       Calculates the size of mdat
*						= 8 + audioFramesSize + videoFramesSize
*
* Parameters    :       [IN] *videoInfo - video info structure
*						[IN] *audioInfo - audio info structure
*
* Return Value  :       Returns the size of mdat
*
*******************************************************************************/
int getMdatSize(VideoInfo *videoInfo, AudioInfo *audioInfo)
{
	int size = 8;	//	4 bytes for mdatSize, 4 for 'm', 'd', 'a', 't'
	int i;

	for(i=0;i<videoInfo->totalNumFrm;i++)
		size+=videoInfo->frameSizeArray[i].frameSize;

	if(audioInfo!=NULL)
		for(i=0;i<audioInfo->totalNumFrm;i++)
			size+=audioInfo->frameSizeArray[i].frameSize;

	return size;	
}

/******************************************************************************
*
* Function      :       generateMp4Footer
*
* Description   :       This function generates the footer, as well as writing
*						the size of 'mdat'. This supports P frames of length 0.
*
* Parameters    :       [IN] *buffer - Pointer to frame buffer
*
* Return Value  :       Returns the size of the footer in bytes on success and
*						-1 on failure
*
*******************************************************************************/
int generateMp4Footer( char *buffer, unsigned int *scratchTable, unsigned
        scratchTableSize, int *mSize, char *errorString, VideoInfo *videoInfo,
        AudioInfo *audioInfo)
{
	int i;
	int footerSizeBytes = 0;	// counter that stores the number of bytes written
	int audioDuration = 0, videoDuration = 0;				// total time duration
	int trakSize, moovSize;
	int audioSamplesPerFrame = 0;

	if( errorString == NULL )
		return MP4_NULLERRORSTRING;

	if(	videoInfo == NULL )
	{
		sprintf( errorString, "VideoInfo is NULL|\n");
		return MP4_ERROR;
	}
	if( videoInfo->H264_Header == NULL )
	{
		sprintf( errorString, "H264_Header is NULL!\n" );
		return MP4_ERROR;
	}

	if( (scratchTable == NULL) || (scratchTableSize < videoInfo->totalNumFrm*sizeof(int)) )
	{
		sprintf( errorString, "scratchTable is NULL or it is too small!" \
				 " Must be at least %d bytes\n", (int)(videoInfo->totalNumFrm*sizeof(int)));
		return MP4_ERROR;
	}

	videoDuration = TIME_SCALE/videoInfo->fps*videoInfo->totalNumFrm;
    if( audioInfo != NULL )
	{
		audioSamplesPerFrame = ( audioInfo->aacLC == 1 ) ? 1024 : 2048;
	    audioDuration = (audioInfo->totalNumFrm * audioSamplesPerFrame/audioInfo->sampleRate)*TIME_SCALE;
	}

	/*** Note Pointer Type ***/
	// Note that if a int *pointer is not 4-byte aligned, then bus error on ARM
	// To avoid this potential problem, use char *pointer

	/* Generating footer */
	// 'moov'
	// moovSize will be updated at the end
	footerSizeBytes+=4;
	WRITE_TYPE(buffer, footerSizeBytes, 'm','o','o','v')
	// 'mvhd'
	WRITE_4BYTES(buffer, footerSizeBytes, MOVIE_HEADER_SIZE)
	WRITE_TYPE(buffer, footerSizeBytes, 'm','v','h','d')
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// version, flags
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// creation time
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// modification time
	WRITE_4BYTES(buffer, footerSizeBytes, TIME_SCALE)
	WRITE_4BYTES(buffer, footerSizeBytes, ((videoDuration > audioDuration) ? videoDuration : audioDuration))
	WRITE_4BYTES(buffer, footerSizeBytes, 0x00010000)	// preferred rate
	WRITE_4BYTES(buffer, footerSizeBytes, 0x01000000)	// 2 byte volume
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// reserved
	WRITE_4BYTES(buffer, footerSizeBytes, 0)			// reserved
	WRITE_4BYTES(buffer, footerSizeBytes, 0x00010000)	// matrix[0,0]
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0x00010000)	// matrix[1,1]
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0)
	WRITE_4BYTES(buffer, footerSizeBytes, 0x40000000)	// matrix[2,2]
	for( i = 0; i < 24; i++ )
		buffer[footerSizeBytes++] = 0;
	WRITE_4BYTES(buffer, footerSizeBytes, (audioInfo != NULL)?3:2)			// next track ID

    //set videoInfo->duration to 0 for fragmented mp4
	// Video Trak
	footerSizeBytes = writeTrak(buffer, footerSizeBytes, videoInfo, audioInfo,
								1, errorString, scratchTable, &trakSize);
	if(footerSizeBytes < 0)
		return MP4_ERROR;
	moovSize = 8 + MOVIE_HEADER_SIZE + trakSize;

	// Audio Trak, if present
	if(audioInfo != NULL)
	{
		footerSizeBytes = writeTrak(buffer, footerSizeBytes, videoInfo, audioInfo,
								  	2, errorString, scratchTable, &trakSize);
		if(footerSizeBytes < 0)
			return MP4_ERROR;
		moovSize += trakSize;
	}

    if (videoDuration == 0 && audioDuration == 0) {
        trakSize = 0;
        footerSizeBytes = writeMvex(buffer, footerSizeBytes, videoInfo, audioInfo, errorString, scratchTable, &trakSize);
        moovSize += trakSize;
    }

    //if videoInfo->duration and audioInfo->duration == 0, don't do this (fmp4)
    if (videoDuration > 0 || audioDuration > 0) {
        *mSize = getMdatSize(videoInfo, audioInfo);
    }

	buffer[0] = (moovSize & 0xFF000000) >> 24;
	buffer[1] = (moovSize & 0x00FF0000) >> 16;
	buffer[2] = (moovSize & 0xFF00) >> 8;
	buffer[3] = (moovSize & 0xFF);

/*************************************

*			TO BE UPDATED

*************************************/

/*************************************
  The size of the footer is:

  609 + STTS + STSZ + STCO + STSS
= 609 +
  (totalNumFrm/2)*4*2 +
  totalNumFrm*4 +
  totalNumFrm*4 +
  ((totalNumFrm/I_frame_int)+1)*4
= 609 + 4T + 8T + 4T/I + 4
= 613 + 12T + 4T/I
 *************************************/
/*************************************
  If the time table is capped,
  the size of the footer is:

  609 + STTS + STSZ + STCO + STSS
= 609 +
  (MAX_TIME_TABLE_ENTRIES)*4*2 +
  totalNumFrm*4 +
  totalNumFrm*4 +
  ((totalNumFrm/I_frame_int)+1)*4
= 609 + 160 + 8T + 4T/I + 4
= 773 + 8T + 4T/I
 *************************************/

	return footerSizeBytes;
}

/******************************************************************************
*
* Function      :       insertFrameSize
*
* Description   :       This function inserts the size of 'mdate' in the header
*						This should be used only if the moov atom is in the 
*						footer
*
* Parameters    :       [IN] mSize - Size of 'mdat'
*
* Return Value  :       Returns the size of the footer in bytes on success and
*						-1 on failure
*
*******************************************************************************/
int insertFrameSize( FILE *inFile, int mSize, char *errorString )
{
	if( errorString == NULL )
		return MP4_NULLERRORSTRING;

	if( inFile == NULL )
	{
		sprintf( errorString, "NULL file pointer!\n" );
		return MP4_ERROR;
	}

	// write the size of 'mdat'
	fseek( inFile, 24, SEEK_SET );
	fputc( (mSize & 0xFF000000) >> 24, inFile );
	fputc( (mSize & 0x00FF0000) >> 16, inFile );
	fputc( (mSize & 0x0000FF00) >> 8, inFile );
	fputc( mSize & 0xFF, inFile );

	return MP4_OK;
}

/******************************************************************************
*
* Function      :       generateMp4Header_w_moov_atom
*
* Description   :       This function creates a place holder of the entire .mp4
*						wrapper. The content will be filled out after the
*                       encoded frames are written.
*
* Parameters    :       [IN] totalNumFrm - Total number of frames
*
* Return Value  :       Returns number of bytes of the MP4 header on success
* 						and MP4_ERROR on failure.
*
*******************************************************************************/
int generateMp4Header_w_moov_atom( FILE *fp, const int totalNumFrm,
								   const int iFrameInterval , 
								   const int numAudioFrames)
{
	return generateMp4Header_w_moov_atom_w_spsppssize (fp, totalNumFrm,
							   iFrameInterval,
							   numAudioFrames, 0);
}

int generateMp4Header_w_moov_atom_w_spsppssize(vector<unsigned char> &output_vector, const int totalNumFrm,
				 		const int iFrameCount, const int numAudioFrames,
						const int spsPpsSize)
{
	int bytesCount = 0;
	char buffer[MP4HEADERSIZE];
	int moovBufferSize = 0;
	char *moovBuffer;

	// 581 + sps + pps + 8T + 4T/I + 8 * stts_entries
	if (spsPpsSize)
		// 581 + spsPpsSize + 8T + 4T/I + 160 (assuming max stts_entries = 20)
		// 741 + spsPpsSize + 8T + 4T/I
		moovBufferSize = (741 + spsPpsSize + 8*totalNumFrm +
		                  4*iFrameCount) * sizeof(char);
	else
		// 594 + 8T + 4T/I + 8 * stts_entries (assuming sps=9, pps=3)
		// 594 + 8T + 4T/I + 160 (assuming max stts_entries = 20)
		// 754 + 8T + 4T/I
		moovBufferSize = (754 + 8*totalNumFrm + 4*iFrameCount 
		                 * sizeof(char));

	if(numAudioFrames > 0)
		moovBufferSize+=(435 + 8 * numAudioFrames);


	// Round up to nearest 4-byte aligned if not already 4-byte aligned
	if( moovBufferSize & 0x3 )
	  moovBufferSize = (moovBufferSize+4) & ~0x3;

	moovBuffer = (char*)malloc(moovBufferSize);

    //if (output_vector.capacity() < moovBufferSize) {
        //output_vector.reserve(moovBufferSize);
    //}

	if( (moovBuffer == NULL) )
		return MP4_ERROR;
	else
		memset(moovBuffer, 0, moovBufferSize);

	WRITE_4BYTES(buffer, bytesCount, FILE_TYPE_SIZE)
	WRITE_TYPE(buffer, bytesCount, 'f','t','y','p')
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_4BYTES(buffer, bytesCount, 0)
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_TYPE(buffer, bytesCount, 'i','s','o','m')
    //WRITE_TYPE(buffer, bytesCount, 'i','s','o','5')

    output_vector.insert(output_vector.end(), buffer, buffer  + MP4HEADERSIZE - (2*4));
	//retVal = fwrite( buffer, MP4HEADERSIZE-(2*4), 1, fp );
	//if( retVal != 1 )
	//{
		//free( moovBuffer );
		//return MP4_ERROR;
	//}

	//'moov'
    //output_vector.insert(output_vector.end(), moovBuffer, moovBuffer + moovBufferSize);
	free( moovBuffer );

	//retVal = fwrite( moovBuffer, moovBufferSize, 1, fp );
	//if( retVal != 1 )
		//return MP4_ERROR;


    // if total frames  == 0, don't write mdat (fragmented mp4)
    
    if (totalNumFrm > 0) {
        WRITE_4BYTES(buffer, bytesCount, 0)	// will insert mdatSize at footer
        WRITE_TYPE(buffer, bytesCount, 'm','d','a','t')
        output_vector.insert(output_vector.end(), buffer  + MP4HEADERSIZE - (2*4), buffer + (MP4HEADERSIZE - (2*4)) + (2*4));
    } else {
        moovBufferSize += MVEXHEADERSIZE;

        if (numAudioFrames > 0) {
            moovBufferSize += TREXSIZE;
        }
    }

	//retVal = fwrite( buffer+MP4HEADERSIZE-(2*4), 2*4, 1, fp );
	//if( retVal != 1 )
		//return MP4_ERROR;
        //

    return MP4HEADERSIZE+moovBufferSize;
}

int generateMp4Header_w_moov_atom_w_spsppssize( FILE *fp, const int totalNumFrm,
				 		const int iFrameInterval , const int numAudioFrames,
						const int spsPpsSize)
{
	int bytesCount = 0;
	char buffer[MP4HEADERSIZE];
	int moovBufferSize = 0;
	char *moovBuffer;
	int retVal;

	// 581 + sps + pps + 8T + 4T/I + 8 * stts_entries
	if (spsPpsSize)
		// 581 + spsPpsSize + 8T + 4T/I + 160 (assuming max stts_entries = 20)
		// 741 + spsPpsSize + 8T + 4T/I
		moovBufferSize = (741 + spsPpsSize + 8*totalNumFrm +
		                  4*totalNumFrm/iFrameInterval) * sizeof(char);
	else
		// 594 + 8T + 4T/I + 8 * stts_entries (assuming sps=9, pps=3)
		// 594 + 8T + 4T/I + 160 (assuming max stts_entries = 20)
		// 754 + 8T + 4T/I
		moovBufferSize = (754 + 8*totalNumFrm + 4*totalNumFrm/iFrameInterval)
		                 * sizeof(char);

	if(numAudioFrames > 0)
		moovBufferSize+=(435 + 8 * numAudioFrames);

	// Round up to nearest 4-byte aligned if not already 4-byte aligned
	if( moovBufferSize & 0x3 )
	  moovBufferSize = (moovBufferSize+4) & ~0x3;

	moovBuffer = (char*)malloc(moovBufferSize);

	if( (fp == NULL) || (moovBuffer == NULL) )
		return MP4_ERROR;
	else
		memset(moovBuffer, 0, moovBufferSize);

	WRITE_4BYTES(buffer, bytesCount, FILE_TYPE_SIZE)
	WRITE_TYPE(buffer, bytesCount, 'f','t','y','p')
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_4BYTES(buffer, bytesCount, 0)
	WRITE_TYPE(buffer, bytesCount, 'm','p','4','2')
	WRITE_TYPE(buffer, bytesCount, 'i','s','o','m')

	retVal = fwrite( buffer, MP4HEADERSIZE-(2*4), 1, fp );
	if( retVal != 1 )
	{
		free( moovBuffer );
		return MP4_ERROR;
	}

	//'moov'
	retVal = fwrite( moovBuffer, moovBufferSize, 1, fp );
	free( moovBuffer );

	if( retVal != 1 )
		return MP4_ERROR;

	WRITE_4BYTES(buffer, bytesCount, 0)	// will insert mdatSize at footer
	WRITE_TYPE(buffer, bytesCount, 'm','d','a','t')

	fwrite( buffer+MP4HEADERSIZE-(2*4), 2*4, 1, fp );
	if( retVal != 1 )
		return MP4_ERROR;

    return MP4HEADERSIZE+moovBufferSize;
}

/******************************************************************************
*
* Function      :       writeMoovAtom
*
* Description   :       This function writes the .mp4 moov atom table to the
*						header of the .mp4 file.
*
* Parameters    :       [IN] *moovBuffer - moov atom buffer
*
* Return Value  :       Returns MP4_OK on success
* 						and MP4_ERROR on failure.
*
*******************************************************************************/
int writeMoovAtom( FILE *fp, char *moovBuffer, int moovBufSize,
				   int mp4HeaderSize, int mSize )
{
	int retVal, writeSize;

	fseek( fp, MP4HEADERSIZE-(2*4), SEEK_SET );

	// mdatSize is at mp4HeaderSize - 8. Make sure we don't write past it
	writeSize = moovBufSize > (mp4HeaderSize -8) ?
		    (mp4HeaderSize - 8) : moovBufSize;
	retVal = fwrite( (void*)moovBuffer, 1, writeSize, fp );
	if( retVal != writeSize )
		return MP4_ERROR;

	fseek( fp, mp4HeaderSize-(4*2), SEEK_SET );

	// write mdatSize
	fputc( (mSize & 0xFF000000) >> 24, fp );
	fputc( (mSize & 0x00FF0000) >> 16, fp );
	fputc( (mSize & 0x0000FF00) >> 8, fp );
	fputc( mSize & 0xFF, fp );

	return MP4_OK;
}


/******************************************************************************
*
* Function      :       writeMoovAtom
*
* Description   :       This function writes the .mp4 moov atom table to the
*						header of the .mp4 file.
*
* Parameters    :       [IN] *moovBuffer - moov atom buffer
*
* Return Value  :       Returns MP4_OK on success
* 						and MP4_ERROR on failure.
*
*******************************************************************************/
int writeMoovAtom(vector<unsigned char> &output_vector, char *moovBuffer, int moovBufSize,
				   int mp4HeaderSize, int mSize )
{
	int writeSize;

	//fseek( fp, MP4HEADERSIZE-(2*4), SEEK_SET );

	// mdatSize is at mp4HeaderSize - 8. Make sure we don't write past it
	writeSize = moovBufSize > (mp4HeaderSize -8) ?
		    (mp4HeaderSize - 8) : moovBufSize;

    output_vector.insert(output_vector.end(), moovBuffer, moovBuffer + writeSize);
    //for (int j = 0, i = MP4HEADERSIZE - (2*4); j < writeSize; ++i, ++j) {
    //}

	//retVal = fwrite( (void*)moovBuffer, 1, writeSize, fp );
	//if( retVal != writeSize )
		//return MP4_ERROR;

	//fseek( fp, mp4HeaderSize-(4*2), SEEK_SET );
    
    // check for numFrames > 0 then write the mdat size
    //i = mp4HeaderSize - (4 * 2);
    //std::cout << i << std::endl;
    //output_vector[i++] = (mSize & 0xFF000000) >> 24;
    //output_vector[i++] = (mSize & 0x00FF0000) >> 16;
    //output_vector[i++] = (mSize & 0x0000FF00) >> 8;
    //output_vector[i++] = (mSize & 0xFF);

	// write mdatSize
	//fputc( (mSize & 0xFF000000) >> 24, fp );
	//fputc( (mSize & 0x00FF0000) >> 16, fp );
	//fputc( (mSize & 0x0000FF00) >> 8, fp );
	//fputc( mSize & 0xFF, fp );

	return MP4_OK;
}

int getFrameStartOffset(unsigned char *buffer, int length)
{
    int i=0;
    unsigned int data=0xffffffff;
    for(i=0;i<length;i++)
    {
        data = (data<<8)|buffer[i];
        // Start of new NAL unit
        if(data==0x00000001)
        {
            // Ignore AUDelimiter(09), SPS(67), PPS(68), SEI(06)
            if(buffer[i+1]!= 0x67 && buffer[i+1]!= 0x68
                    && buffer[i+1]!= 0x09 && buffer[i+1]!= 0x06)
            {
                //printf("returning %d\n", (i+1));fflush(stdout);
                return (i+1);
            }
        }
    }
    return -1;
}


void writeFragment(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, VideoInfo *videoInfo, AudioInfo *audioInfo, unsigned seq_num, unsigned int *video_pts_index, unsigned int *audio_pts_index) 
{

    // TODO extend for both video and audio
    writeMoof(output_vector, frame_vector, videoInfo, audioInfo, seq_num, video_pts_index, audio_pts_index);
    writeMdat(output_vector, frame_vector, videoInfo->frameSizeArray, videoInfo->totalNumFrm);
}

void writeMoof(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, VideoInfo *videoInfo, AudioInfo *audioInfo, unsigned seq_num, unsigned int *video_pts_index, unsigned int *audio_pts_index)
{
    /* Format:
     * moof size
     * moof
     * mfhd size
     * mfhd
     * (version, flags) = 0
     * seq num
     * traf size
     * traf
     * tfhd size
     * tfhd
     * (version, tf_flags)
     * track_id (1 for vid 2 for aud)
     * default_duration = 90090/FRAME_RATE
     * default_sample_flags (based on trun flags pg 53 probably not needed)
     * tfdt size
     * tfdt
     * trun size
     * trun
     * version = 0
     * tr_flags (based on optional data)
     * sample_count (numFrames)
     * data_offset
     * for each sample (duration, size, flags) 
     */
    unsigned moofStart;
             
    unsigned moofSize;
    int dataOffsetStart = -1;
    char byteArray[4];

    moofStart = output_vector.size();

    writePlaceholderBytes(output_vector, 4);
    writeString(output_vector, "moof");


    // MFHD Size = 4 (size) + 4 (header) + 4 (version, flags) + 4 (seq_num) = 16

    numToBytes(byteArray, MFHDSIZE);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    writeString(output_vector, "mfhd");
    numToBytes(byteArray, 0);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    numToBytes(byteArray, seq_num);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    if (videoInfo != NULL) {
        dataOffsetStart = writeTraf(output_vector, frame_vector, videoInfo->frameSizeArray, 1, videoInfo->totalNumFrm, TIME_SCALE/videoInfo->fps * videoInfo->totalNumFrm, pts_video, video_pts_index);
        pts_video += (TIME_SCALE/videoInfo->fps) * videoInfo->totalNumFrm;
    }

    if (audioInfo != NULL) {
        writeTraf(output_vector, frame_vector, audioInfo->frameSizeArray, 2, audioInfo->totalNumFrm, audioInfo->sampleRate, pts_audio, audio_pts_index);
        // TODO increment pts_audio
    }


    moofSize = output_vector.size() - moofStart;
    numToBytes(byteArray, moofSize);
    copy(byteArray, byteArray + 4, output_vector.begin() + moofStart);

    if (dataOffsetStart != -1) {
        numToBytes(byteArray, moofSize + MDATHEADERSIZE);
        copy(byteArray, byteArray + 4, output_vector.begin() + dataOffsetStart);
    }


}

void writeMdat(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo *frame_size_array, unsigned frame_count)
{
    // Format:
    // mdat size
    // mdat
    // for each frame:
    // frameSize
    // frame without start code
    // mdatSize = 4 + 4 + (for i in frames: ((4 + (frameSize[i])
    unsigned mdatStart = output_vector.size(), mdatSize;
    unsigned char *current_frame;
    struct frameInfo frame_info;
    char byteArray[4];


    writePlaceholderBytes(output_vector, 4);
    writeString(output_vector, "mdat");

    for (unsigned i = 0; i < frame_count; ++i) {
        frame_info = frame_size_array[i];
        current_frame = frame_vector.data() + frame_info.startIndex;

        numToBytes(byteArray, frame_info.frameSize);

        output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

        output_vector.insert(output_vector.end(), current_frame, current_frame + frame_info.frameSize);

    }

    mdatSize = output_vector.size() - mdatStart;
    numToBytes(byteArray, mdatSize);
    copy(byteArray, byteArray + 4, output_vector.begin() + mdatStart);

}

int writeTraf(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo *frame_size_array, unsigned track_id, unsigned totalNumFrm, unsigned track_duration, unsigned long pts, unsigned int *pts_index) 
{
    unsigned trafStart, trafSize, dataOffsetStart;
    char byteArray[4];

    trafStart = output_vector.size();
    writePlaceholderBytes(output_vector, 4);
    writeString(output_vector, "traf");

    writeTfhd(output_vector, track_id, track_duration);
    writeTfdt(output_vector, pts, pts_index);

    dataOffsetStart = writeTrun(output_vector, frame_vector, frame_size_array, totalNumFrm);


    trafSize = output_vector.size() - trafStart;
    numToBytes(byteArray, trafSize);
    copy(byteArray, byteArray + 4, output_vector.begin() + trafStart);

    return dataOffsetStart;
}

void writeTfhd(vector<unsigned char> &output_vector, unsigned track_id, unsigned track_duration)
{
    // tf_flags set to default-sample-duration-present, default-sample-flags-present, 
    unsigned tf_flags = 0x0000008 | 0x0000020 | 0x0020000;
    // default_sample_flags = sample_depends_on, sample_is_non_sync
    unsigned default_sample_flags = 0x01010000;
    char byteArray[4];

    // TFHDSIZE = 8 (header) + 4 (version, flags) + 4 (track_id) + 4 (duration) + 4 (sample_flags)
    numToBytes(byteArray, TFHDSIZE);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);
    writeString(output_vector, "tfhd");

    numToBytes(byteArray, tf_flags);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    numToBytes(byteArray, track_id);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    numToBytes(byteArray, track_duration);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    numToBytes(byteArray, default_sample_flags);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);
}

void writeTfdt(vector<unsigned char> &output_vector, unsigned long pts, unsigned int *pts_index)
{
    char byteArray[4], longByteArray[8];

    numToBytes(byteArray, TFDTSIZE);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);
    writeString(output_vector, "tfdt");
    numToBytes(byteArray, 0x01000000);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    if (pts_index) {
        *pts_index = output_vector.size();
    }

    longToBytes(longByteArray, pts);
    output_vector.insert(output_vector.end(), longByteArray, longByteArray + 8);
}

int writeTrun(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo *frame_size_array, unsigned sample_count)
{
    unsigned trunStart = output_vector.size();
    int writeOffset;
    
    unsigned dataOffsetStart, trunSize, new_frame_size;

    // trun size = 8 (header) + 4 (version, flags) + 4 (sample_count) + 4 (data_offset) + for each sample { 4 (size) + 4 (flags) }
    // tr_flags = data-offset-present, sample-size-present, sample-flags-present
    unsigned tr_flags = 0x000001 | 0x000200 |  0x000400;
    unsigned sample_flags;
    unsigned char *current_frame;
    struct frameInfo frame_info;
    char byteArray[4];

    writePlaceholderBytes(output_vector, 4);
    writeString(output_vector, "trun");
    numToBytes(byteArray, tr_flags);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    numToBytes(byteArray, sample_count);
    output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

    dataOffsetStart = output_vector.size();
    writePlaceholderBytes(output_vector, 4);

    for (unsigned i = 0; i < sample_count; ++i) 
    {
        frame_info = frame_size_array[i];
        current_frame = frame_vector.data() + frame_info.startIndex;
        if ((writeOffset = getFrameStartOffset(current_frame, frame_info.frameSize)) < 0) {
            continue;
        }

        // last index = startIndex + frameSize
        //
        new_frame_size = frame_info.frameSize - (writeOffset - frame_info.startIndex);
        frame_info.startIndex = writeOffset;
        frame_info.frameSize = new_frame_size;

        numToBytes(byteArray, frame_info.frameSize + 4);
        output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

        if (frame_info.IFrame == 1) {
            // iframe flags: is_leading (3), sample_depends_on (2)
            sample_flags = 0xc000000;
        } else {
            sample_flags = 0x1010000;
        }

        numToBytes(byteArray, sample_flags);
        output_vector.insert(output_vector.end(), byteArray, byteArray + 4);

        frame_size_array[i] = frame_info;
    }

    trunSize = output_vector.size() - trunStart;
    numToBytes(byteArray, trunSize);
    copy(byteArray, byteArray + 4, output_vector.begin() + trunStart);

    return dataOffsetStart;
}

void setBaseMediaDecodeTime(vector<unsigned char> &output_vector, unsigned long pts)
{
    const char *tfdt_header = "tfdt";
    char byteArray[8];

    auto it = search(output_vector.begin(), output_vector.end(), tfdt_header, tfdt_header + 4);

    if (it != output_vector.end()) {
        it += 8;

        longToBytes(byteArray, pts);
        copy(byteArray, byteArray + 8, it);
    }
}

void writePlaceholderBytes(vector<unsigned char> &output_vector, size_t count) 
{
    for (unsigned i = 0; i < count; ++i) {
        output_vector.push_back(0x0);
    }
}

void writeString(vector<unsigned char> &output_vector, const string value)
{

    for (unsigned i = 0; i < value.size(); ++i) {
        output_vector.push_back(value[i]);
    }
}

void numToBytes(char byteArray[4], unsigned num)
{
    byteArray[0] = (num & 0xFF000000) >> 24;
    byteArray[1] = (num & 0x00FF0000) >> 16;
    byteArray[2] = (num & 0x0000FF00) >> 8;
    byteArray[3] = (num & 0xFF);
}

void longToBytes(char byteArray[8], unsigned long num)
{
    for (unsigned i = 0; i < 8; ++i) {
        byteArray[7 - i] = ((num >> (8 * i)) & 0xFF);
    }
    //byteArray[0] = (num & 0xFF000000) >> 24;
    //byteArray[1] = (num & 0x00FF0000) >> 16;
    //byteArray[2] = (num & 0x0000FF00) >> 8;
    //byteArray[3] = (num & 0xFF);
}
