/*
 * H264_TS_Packetizer.c
 *
 * Copyright (c) 2009 Cernium Corporation
 * All Rights Reserved.
 *
 *
 * Author: Lai-Tee Cheok
 *
 */
#include <vector>

#include "H264_TS_Packetizer.hh"
#include "Debug.h"
#include <iostream>

#define TSMAKER_ENHANCED       1
#ifdef SINGLE_PES_PER_FRAME
#include <stdlib.h>
#include <errno.h>
// Assuming a max bitrate of 8 Mbits/s, a single frame should definitely
// be less that 1 MB
#define MAX_FRAME_SIZE (1024*1024)
#endif
// Compile time debugging option support
extern unsigned int dbgLevel;

char inputFileName[100] ;

unsigned long pts_val = 63000L;
int cc = 0;

// initialize variables, open input, output files, set frame rate
//
/*
 * Update: lt_init to use buffers (vectors are ok), getNextFrame to use buffers, lt_write_bits to use buffers
 */
int current_bit_pos;
unsigned char buf[TS_BUFFER_SIZE+3];
int cur_ts_pkt_bytes;
FILE *m_file;
unsigned long bytes_to_write;
unsigned long pts_incr;
int first_ts_pkt;
int prev_PPS;
unsigned int frame_rate;
int fd;
int buf_len;
unsigned int total_bits;
unsigned int last_bits;
ERROR_TYPE ErrCode;
std::vector<uint8_t> *output_vect;

using namespace std;

int lt_init(vector<uint8_t> *output, int fr) 
{
    bytes_to_write = 0L;
    //pts_incr = 0;
    first_ts_pkt = 1;
    prev_PPS = 0;
    fd = 0;
    cc = 0;

    //m_file = fopen("dump.ts", "ab");
    current_bit_pos=0;
    total_bits=0;
    buf_len=TS_BUFFER_SIZE;
    memset(buf, 0, TS_BUFFER_SIZE);
    output_vect = output;

    if ( fr <= 0 ) {
        PRINT_ERR("%s : Warning: frame rate specified is negative or 0, default 30fps is used.\n",__FUNCTION__);
        frame_rate = FRAME_RATE;
    }
    else
        frame_rate = fr;

    //pts_incr = (unsigned long)((1.0/(double)frame_rate)*(double)90000);
    pts_incr = 90090 / frame_rate;


	return 1;
}

int lt_frame_init()
{
    bytes_to_write = 0L;
    fd = 0;

    current_bit_pos=0;
    total_bits=0;
    buf_len=TS_BUFFER_SIZE;
    memset(buf, 0, TS_BUFFER_SIZE);



	return 1;
}

int lt_init(const char *input_file, const char *output_file, int fr)
{
    bytes_to_write = 0L;
    pts_incr = 0;
    first_ts_pkt = 1;
    prev_PPS = 0;
    fd = 0;
    cc = 0;

    current_bit_pos=0;
    total_bits=0;
    buf_len=TS_BUFFER_SIZE;
    memset(buf, 0, TS_BUFFER_SIZE);

	m_file = fopen(input_file, "rb");

	if (m_file == NULL) {
		PRINT_ERR("%s : input file is not found\n",__FUNCTION__);
		return -1;
	}

    strcpy(inputFileName, input_file );

	if ( (fd=open(output_file, O_WRONLY|O_CREAT|O_TRUNC, 0777)) < 0  ) {
		PRINT_ERR("%s : can't open file for writing TS stream!\n",__FUNCTION__);
    fclose(m_file);
		return -1;
	}

	//setmode(fd,O_BINARY);

	if ( fr <= 0 ) {
		PRINT_ERR("%s : Warning: frame rate specified is negative or 0, default 30fps is used.\n",__FUNCTION__);
		frame_rate = FRAME_RATE;
	}
	else
		frame_rate = fr;

	pts_incr = (unsigned long)((1.0/(double)frame_rate)*(double)90000);

	return 1;
}

// clean up before exit
void lt_cleanup(void)
{
	lt_flush_bits();
	if ( m_file != NULL )
		fclose(m_file);
	if ( fd != 0 )
		close(fd);
}

int lt_packetize(vector<uint8_t> frame_buffer, vector<struct frameInfo> frame_size_array, bool ts_headers) 
{
    int ret = 0;

    if (ts_headers) {
        lt_create_PAT();
        lt_create_PMT();
    }

    for (auto it: frame_size_array) {
        if ((ret = lt_write_data_pkts(frame_buffer.data() + it.startIndex, it.frameSize)) == -1 || ret == -2) {
            DBG_PRINT(DBG_LEVEL_5,"TSM: lt_write_data_pkts exited without completing ret %d \n", ret);
        }
    }
    return ret;
}
// start packetizing
int lt_packetize()
{
	int ret =0;
	lt_create_PAT();
	lt_create_PMT();
	ret = lt_write_data_pkts();
	if((ret == -1) || (ret == -2)){
        DBG_PRINT(DBG_LEVEL_5,"TSM: lt_write_data_pkts exited without completing ret %d \n", ret);
	}
    return ret;
}

// writes TS header
void lt_write_ts_header(int payload_indicator, int pid, int adapt_field_ctrl, int cc)
{
	int sync_bytes = 0x47;
	int transport_error_indicator = 0;
	int transport_priority = 0;
	int transport_scrambling_control = 0;
	int pgm_clk_ref = 0;

	lt_write_bits(sync_bytes,8); //output sync 0x47 bytes
	lt_write_bits(transport_error_indicator,1);  //output transport_error_indicator

	lt_write_bits(payload_indicator,1);	//payload indicator
	lt_write_bits(transport_priority,1);	//transport priority
	lt_write_bits(pid,13);	//PID
	lt_write_bits(transport_scrambling_control,2);	//transport scrambling control
	lt_write_bits(adapt_field_ctrl,2);
	lt_write_bits(cc,4);

	cur_ts_pkt_bytes = 4;

	if ( (adapt_field_ctrl == 2) || (adapt_field_ctrl == 3) )
		lt_write_adaptation_field(pgm_clk_ref);

}

// write adaptation field of TS header
void lt_write_adaptation_field(int pgm_clk_ref)
{
	int adapt_field_len = 0;

	int discontinuity_indicator = 0;
	int random_access_indicator = 0;
	int ES_priority_indicator = 0;
	int pcr_flag = 0;
	int OPCR_flag = 0;
	int splicing_point_flag = 0;
	int transport_private_data_flag = 0;
	int transport_private_data_len = 0;
	int adaptation_field_extension_flag = 0;
	int ltw_flag = 0;
	int piecewise_rate_flag = 0;
	int seamless_splice_flag = 0;

	int bytes_to_stuff = 0;

	if ( pgm_clk_ref != 0 )
		pcr_flag = 1;

	adapt_field_len += 1;  //for discontinuity_counter, etc.

	if ( pcr_flag == 1)  //for pcr_flag
		adapt_field_len += 6;

	if ( OPCR_flag == 1 ) //for OPCR_flag
		adapt_field_len += 6;

	if ( splicing_point_flag == 1 ) //for splicing_point_flag
		adapt_field_len += 1;

	if ( transport_private_data_flag == 1 ) {
		adapt_field_len += 1;
		adapt_field_len += transport_private_data_len;
	}

	if ( adaptation_field_extension_flag == 1 ) {
		adapt_field_len += 2;
		if ( ltw_flag == 1 )
			adapt_field_len += 2;
		if ( piecewise_rate_flag == 1 )
			adapt_field_len += 3;
		if ( seamless_splice_flag == 1)
			adapt_field_len += 5;

	}

	bytes_to_stuff = 188 - (adapt_field_len + 1 + 4) - bytes_to_write;
	if ( bytes_to_stuff > 0 )  //only if there're stuffing bytes
		adapt_field_len += bytes_to_stuff;

	lt_write_bits(adapt_field_len,8); //adapt_field_len
	lt_write_bits(discontinuity_indicator,1); //discontinuity_indicator
	lt_write_bits(random_access_indicator,1); //random_access_indicator
	lt_write_bits(ES_priority_indicator,1); //ES priority indicator
	lt_write_bits(pcr_flag,1); //PCR flag
	lt_write_bits(OPCR_flag,1); //OPCR flag
	lt_write_bits(splicing_point_flag,1); //splicing_point_flag
	lt_write_bits(transport_private_data_flag,1); //transport_private_data_flag
	lt_write_bits(adaptation_field_extension_flag,1); //adaptation_field_extension_flag

	cur_ts_pkt_bytes += 2;

	if ( pcr_flag == 1) {
		lt_write_bits(pgm_clk_ref,33); //program clock reference
		lt_write_bits(0,6); //reserved
		lt_write_bits(0,9); //pgm clk ref extension
		cur_ts_pkt_bytes += 6;
	}

	if ( bytes_to_stuff > 0 )
		lt_stuff_bytes(bytes_to_stuff);
}

// stuff bytes
void lt_stuff_bytes(int num_bytes)
{
	int i = 0;

	for (i=0; i<num_bytes; i++) {
		lt_write_bits(255,8);  //0xFF
		cur_ts_pkt_bytes++;
	}
}

// creates PAT include stuffing bytes if necessary
void lt_create_PAT()
{
	cur_ts_pkt_bytes = 0;
	lt_write_ts_header(1, 0, 1, 0); //payload_indicator, pid, adapt_field_ctrl, cc
	{
		lt_write_bits(0, 8);
		cur_ts_pkt_bytes += 1;
	}
	lt_write_PAT(0, 1, PGM_NUM, PGM_MAP_PID); //table_id, ts_id, pgm_num, pgm_map_pid
	lt_stuff_bytes(188-cur_ts_pkt_bytes);
}

// writes PAT
void lt_write_PAT(int table_id, int ts_id, int pgm_num, int pgm_map_pid)
{
	int section_syntax_indicator = 1;
	int reserved_1 = 3;
	int reserved_2 = 3;
	int reserved_3 = 7;
	int section_length = 13;
	int version_num = 0;
	int current_next_indicator = 1;
	int section_num = 0;
	int last_section_num = 0;

	lt_write_bits(table_id,8); //table id
	lt_write_bits(section_syntax_indicator,1);  //section syntax indicator
	lt_write_bits(0,1);
	lt_write_bits(reserved_1,2);
	lt_write_bits(section_length,12);
	lt_write_bits(ts_id,16);
	lt_write_bits(reserved_2,2);
	lt_write_bits(version_num,5); //version number
	lt_write_bits(current_next_indicator,1);
	lt_write_bits(section_num,8);
	lt_write_bits(last_section_num,8);

	cur_ts_pkt_bytes += 8;

	{
		lt_write_bits(pgm_num,16);	//program number
		lt_write_bits(reserved_3,3);
		lt_write_bits(pgm_map_pid,13);  //program map PID
		cur_ts_pkt_bytes += 4;
	}

	lt_write_bits(54,8);
	lt_write_bits(144,8);
	lt_write_bits(226,8);
	lt_write_bits(61,8);

	cur_ts_pkt_bytes += 4;

}

// creates PMT, includes stuffing bytes if necessary
void lt_create_PMT()
{
	cur_ts_pkt_bytes = 0;
	lt_write_ts_header(1, PGM_MAP_PID, 1, 0);  //payload_indicator, pid, adapt_field_ctrl, cc
	{
		lt_write_bits(0, 8);
		cur_ts_pkt_bytes += 1;
	}
	lt_write_PMT(2, PGM_NUM, PCR_PID, ES_PID); //table id, program number, pcr pid, es pid
	lt_stuff_bytes(188-cur_ts_pkt_bytes);
}

// writes PMT
void lt_write_PMT(int table_id, int pgm_num, int pcr_pid, int es_pid)
{
	int section_syntax_indicator = 1;
	int reserved_1 = 3;
	int reserved_2 = 3;
	int reserved_3 = 7;
	int section_length = 18;
	int version_num = 0;
	int current_next_indicator = 1;
	int section_num = 0;
	int last_section_num = 0;
	int reserved_4 = 15;
	int pgm_info_len = 0;
	int stream_type = 27; //0x1B
	int es_info_len = 0;

	lt_write_bits(table_id,8);   //table id
	lt_write_bits(section_syntax_indicator,1);
	lt_write_bits(0,1);
	lt_write_bits(reserved_1,2);
	lt_write_bits(section_length,12);
	lt_write_bits(pgm_num,16);   //program number
	lt_write_bits(reserved_2,2);
	lt_write_bits(version_num,5);  //version number
	lt_write_bits(current_next_indicator,1);
	lt_write_bits(section_num,8);
	lt_write_bits(last_section_num,8);

	cur_ts_pkt_bytes += 8;

	lt_write_bits(reserved_3,3);
	lt_write_bits(pcr_pid,13);
	lt_write_bits(reserved_4,4);
	lt_write_bits(pgm_info_len,12);

	cur_ts_pkt_bytes += 4;

	{
		lt_write_bits(stream_type,8);    //stream type
		lt_write_bits(reserved_3,3);
		lt_write_bits(es_pid,13);        //ES pid
		lt_write_bits(reserved_4,4);
		lt_write_bits(es_info_len,12);

		cur_ts_pkt_bytes += 5;
	}

	lt_write_bits(21,8);
	lt_write_bits(189,8);
	lt_write_bits(77,8);
	lt_write_bits(86,8);

	cur_ts_pkt_bytes += 4;

}

// writes PES header
void lt_write_pes_header(unsigned long pts, unsigned int pts_dts_flags)
{
	int stream_id = 224;
	int pes_pkt_len;
	int pes_scrambling_ctrl = 0;
	int pes_priority = 0;
	int data_alignment_indicator = 0;
	int coyright = 0;
	int original_or_copy = 0;
	int ESCR_flag = 0;
	int ES_rate_flag = 0;
	int DSM_trick_mode_flag = 0;
	int additional_copy_info_flag = 0;
	int PES_CRC_flag = 0;
	int PES_extension_flag = 0;
	int PES_header_data_len = 10;
	int PES_private_data_flag = 0;
	int pack_header_field_flag = 0;
	int program_packet_sequence_counter_flag = 0;
	int P_STD_buffer_flag = 0;
	int PES_extension_flag_2 = 0;
	int PES_extension_field_length = 0;
	int i = 0;

	//compute pes_pkt_len
	pes_pkt_len = PES_PAYLOAD_SIZE + 3;

	if (pts_dts_flags == 0)
		PES_header_data_len = 0;    //update PES header data length
	else if (pts_dts_flags == 2) {
		pes_pkt_len += 5;
		PES_header_data_len = 5;	//update PES header data length
	}
	else if (pts_dts_flags == 3) {
		pes_pkt_len += 10;
		PES_header_data_len = 10;	//update PES header data length
	}

	if ( ESCR_flag == 1)
		pes_pkt_len += 6;

	if ( ES_rate_flag == 1)
		pes_pkt_len += 3;

	if ( DSM_trick_mode_flag == 1) {
		pes_pkt_len += 1; //trick_mode_control
	}

	if ( additional_copy_info_flag == 1 )
		pes_pkt_len += 1;

    if ( PES_CRC_flag == 1)
		pes_pkt_len += 2;

	if ( PES_extension_flag == 1) {
		pes_pkt_len += 1;   //for PES_private_data_flag, etc.

		if ( PES_private_data_flag == 1)
			pes_pkt_len += 16;
		if ( pack_header_field_flag == 1)
			pes_pkt_len += 1;
		if ( program_packet_sequence_counter_flag == 1)
			pes_pkt_len += 2;
		if ( P_STD_buffer_flag == 1)
			pes_pkt_len += 2;
		if ( PES_extension_flag_2 == 1) {
			pes_pkt_len += 1;

			for (i=0; i<PES_extension_field_length; i++)
				pes_pkt_len += 1;
		}

	}

	lt_write_bits(1,24);
	lt_write_bits(stream_id,8); //stream id
#ifdef SINGLE_PES_PER_FRAME
	lt_write_bits(0,16); //PES packet len
#else
	lt_write_bits(pes_pkt_len,16); //PES packet len

#endif

	cur_ts_pkt_bytes += 6;

	lt_write_bits(2,2); //'10'
	lt_write_bits(pes_scrambling_ctrl,2); //PES scrambling ctrl
	lt_write_bits(pes_priority,1); //PES priority
	lt_write_bits(data_alignment_indicator,1); //PES data alignment indicator
	lt_write_bits(coyright,1); //copyright flag
	lt_write_bits(original_or_copy,1);

	cur_ts_pkt_bytes += 1;

	lt_write_bits(pts_dts_flags,2); //PTS DTS flag
	lt_write_bits(ESCR_flag,1);
	lt_write_bits(ES_rate_flag,1);
	lt_write_bits(DSM_trick_mode_flag,1);  //trick mode flag
	lt_write_bits(additional_copy_info_flag,1);
	lt_write_bits(PES_CRC_flag,1);
	lt_write_bits(PES_extension_flag,1);

	cur_ts_pkt_bytes += 1;

	lt_write_bits(PES_header_data_len,8);
	cur_ts_pkt_bytes += 1;

	if ( pts_dts_flags == 2 ) {
		lt_write_pts(2, pts);
		cur_ts_pkt_bytes += 5;   //ok to update cur_ts_pkt_bytes here since not updated in front
	}
	else if ( pts_dts_flags == 3 ) {
		lt_write_pts(3, pts);
		lt_write_pts(1, pts);
		cur_ts_pkt_bytes += 10; //ok to update cur_ts_pkt_bytes here since not updated in front
	}

}


/****************************************************************************************
 * Generate PES header information in TS buffer
 ***************************************************************************************/
void PES_header_info(unsigned char *prefix_buffer, int *first_time, unsigned char *tmp_buf, int *buf_index,
					 unsigned long pts_val, int put_pts, int *VCL, int *prev_PPS)
{
	unsigned char code_buffer[6] = {0x00, 0x00, 0x00, 0x01, 0x09, 0xE0};
	int code_buf_index = 0;

	 //perfix_buffer[4] is stream id, id = 6: H.222.1 type C
#ifndef AMBARELLA_MODE
        // We do not need to add the AUD for Ambarella as it is already present
	if ( ((prefix_buffer[4] & 0x1F) != 6 ) && *first_time ) {
		memcpy(&tmp_buf[*buf_index], code_buffer, 6);
		*buf_index += 6;
	}
#endif
	if (((prefix_buffer[4] & 0x1F) == 1) || ((prefix_buffer[4] & 0x1F) == 5) ) //type type B
		(*VCL)++;
#ifdef AMBARELLA_MODE
    return;
#else
	if (((prefix_buffer[4] & 0x1F) == 8 ) ) //type E
		*prev_PPS = 2;

	if (!(( ((prefix_buffer[4] & 0x1F) == 7 ) && *first_time ) ||
			((prefix_buffer[4] & 0x1F) == 8 ) || (*prev_PPS > 0 )) ) {
		if ( *buf_index + 6 > PES_PAYLOAD_SIZE ) {
			memcpy(&tmp_buf[*buf_index], code_buffer, PES_PAYLOAD_SIZE - *buf_index);
			code_buf_index += (PES_PAYLOAD_SIZE - *buf_index);
			lt_write_ts_pkts(tmp_buf, pts_val, put_pts);
			*buf_index = 0;
			memcpy(&tmp_buf[*buf_index], code_buffer + code_buf_index, 6 - code_buf_index);
			code_buf_index = 0;
			*buf_index += 6 - code_buf_index;
		}
		else {
			memcpy(&tmp_buf[*buf_index], code_buffer, 6);
			*buf_index += 6;
		}
	}

	if ( ((prefix_buffer[4] & 0x1F) == 7 ) && *first_time )
		*first_time = 0;
#endif
}


// read NAL and writes data packets
/*********************************************************************************************
 * Copy PES payload data from H264 buffer to TS output buffer
 *********************************************************************************************/

void PES_payload_data(unsigned char *buffer, unsigned int *buffer_on, unsigned char *tmp_buf, int *buf_index,
					  unsigned long *nbytes, unsigned long pts_base, unsigned long *pts_val,
					  int *put_pts, int VCL)
{
	int nal_bytes_to_write = *nbytes;
	while ( nal_bytes_to_write > 0 ) {

		if ( *buf_index == 0)
			*pts_val = pts_base + (pts_incr*VCL);

		if ( *buf_index + nal_bytes_to_write < PES_PAYLOAD_SIZE ) {
			memcpy(&tmp_buf[*buf_index], buffer + (*buffer_on), nal_bytes_to_write);
			*buf_index += nal_bytes_to_write;
			*buffer_on += nal_bytes_to_write;
			nal_bytes_to_write = 0;
			*put_pts = 1;
		}
		else {
			memcpy(&tmp_buf[*buf_index], buffer + (*buffer_on), PES_PAYLOAD_SIZE - (*buf_index));
			*buffer_on += (PES_PAYLOAD_SIZE - (*buf_index));
			nal_bytes_to_write -= (PES_PAYLOAD_SIZE - (*buf_index));
			lt_write_ts_pkts(tmp_buf, *pts_val, *put_pts);
			*buf_index = 0;
			*put_pts = 0;
		}
	}
	*nbytes = nal_bytes_to_write;
}


/************************************************************************************
 * Check the end of input file to avoid the accessing invalid memory during H264 to TS
 * conversion.
 * The detection condition was derived from function lt_get_next_start_code
 ************************************************************************************/
int test_end_of_file(unsigned int buffer_size, unsigned int buffer_on)
{
	int crash_flag = 0;

    if (buffer_size != MAX_BUFFER - buffer_on && buffer_size < 4){
			crash_flag = 1;
  	                //PRINT_INFO("#10 Write TS: %d  %d   %d   %d\n", buf_index, pts_val, put_pts, buffer_size);
	}
	return(crash_flag);
}

#ifdef SINGLE_PES_PER_FRAME
void printBits(unsigned char *buf, unsigned int len)
{
  for(unsigned i=0;i<len;i++)
    printf("0x%02x\t",buf[i]);
  printf("\n");
}

unsigned int getNextFrame(unsigned char *buffer)
{
  int frameLength=0;
  unsigned char tmp[4];
  int ret = 0;

  ret = fread(tmp, 1, 4, m_file);
  if(ret!=4)
  {
    if(!feof(m_file))
      PRINT_ERR("%s : Unable to read file size from i/p file errno %d\n", __FUNCTION__, errno);
    return 0;
  }
  //printBits(tmp, 4);
  // Convert from big endian
  frameLength = ((tmp[0] << 24) | (tmp[1] << 16) | (tmp[2] << 8) | tmp[3]);
  //PRINT_INFO("Frame Size %d\n", frameLength);
  // @todo - add check for framelength against buffer size
  ret = fread(buffer, 1, frameLength, m_file);
  if(ret!=frameLength)
  {
    PRINT_ERR("%s : Read only %d bytes of frame\n", __FUNCTION__, ret);
    return 0;
  }

  return frameLength;

  // @todo - some way to validate that this is a proper frame

}

int lt_write_data_pkts()
{
  unsigned char *buffer=NULL;
  unsigned int fileLen, bufSize = MAX_FRAME_SIZE, frameLength=0;
  //unsigned long pts_base = 63000L;
  // Length
  fseek(m_file, 0, SEEK_END);
  fileLen = ftell(m_file);
  fseek(m_file, 0, SEEK_SET);

  // No point in malloc a buffer that is bigger than the entire file
  if(fileLen<MAX_FRAME_SIZE)
    bufSize = fileLen;

  buffer = (unsigned char *)malloc(bufSize*sizeof(unsigned char));
  if(buffer==NULL)
  {
    PRINT_ERR("%s : Unable to allocate memory\n", __FUNCTION__);
    return -1;
  }

  while(1)//@todo - add proper check
  {
      frameLength = getNextFrame(buffer);
      if(frameLength==0)
        break;
      // @todo check for aud
      pts_val+= 9009;   // @todo - fix , based on frame rate
      lt_write_ts_pkts(buffer, frameLength, pts_val);

  }//Main while loop
  free(buffer);
  return 0;
}

/*
 * TODO pts_val?
 */
int lt_write_data_pkts(unsigned char *frame_buffer, unsigned int frame_size) 
{
    lt_write_ts_pkts(frame_buffer, frame_size, pts_val);
    //cout << pts_val << endl;
    pts_val += pts_incr;

    return 0;
}

#else
// read NAL and writes data packets
int lt_write_data_pkts()
{
	unsigned char buffer[MAX_BUFFER];
	unsigned int buffer_on = 0;
	unsigned int buffer_size = 0;
	unsigned long bytes = 0;
	unsigned int nal_len = 0;
	unsigned char *prefix_buffer = NULL;
	unsigned int ret;
	int done;
	unsigned char tmp_buf[PES_PAYLOAD_SIZE];
	int buf_index = 0;

//	char code_buffer[6] = {0x00, 0x00, 0x00, 0x01, 0x09, 0xE0};
//	int code_buf_index = 0;
	int VCL = 0;
	int prev_PPS = 0;
	unsigned long nal_bytes_to_write = 0L;
	unsigned long pts_base = 63000L;
	unsigned long pts_val = pts_base;
	int put_pts = 1;
	int first_time = 1;
	int remaining_bytes = 0;
	int err_counter = 0;
    int crash_flag =0;



	while (!feof(m_file)) {
		bytes += buffer_on;
		if (buffer_on != 0) {
			buffer_on = buffer_size - buffer_on;
			memmove(buffer, &buffer[buffer_size - buffer_on], buffer_on);
		}
		remaining_bytes = MAX_BUFFER - buffer_on;
		buffer_size = fread(buffer + buffer_on, 1, MAX_BUFFER - buffer_on, m_file);
		if (crash_flag= test_end_of_file(buffer_size, buffer_on)){
			/* when there are remaining bytes in H264 buffer, copy them to TS buffer */
			if (buffer_size>0 && buffer_on==0){
				nal_bytes_to_write = buffer_size;
				PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
						  &nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);

			}

			lt_write_ts_pkts(tmp_buf, pts_val, put_pts);
			return(1);
		}

		if (remaining_bytes > MAX_BUFFER || crash_flag){
			//DBG_PRINT(DBG_LEVEL_5,"#6  buffer_size, buffer_on =%d   %d\n", buffer_size, buffer_on);
		}


		buffer_size += buffer_on;
		buffer_on = 0;

		done = 0;


		do {
			err_counter  = 0;
			ret = lt_get_next_start_code(buffer + buffer_on,
				  buffer_size - buffer_on, crash_flag);
			//printf("ret, nal_len = %d  ", ret);
			prefix_buffer = buffer + buffer_on;

			if (ret == 0) {
				if ((buffer_size - buffer_on)<10){// the remaining buffer is small
					printf("warning nal_bytes = %d\n", nal_bytes_to_write);
					done = 1;
					break;
				}

				err_counter++;
				// convert the rest of h264 bitstream to TS format
				PES_header_info(prefix_buffer, &first_time, tmp_buf, &buf_index,pts_val,
					put_pts, &VCL,&prev_PPS);
				prev_PPS--;

				if (feof(m_file)) {
					//printf(" I am here");
					nal_bytes_to_write = buffer_size - buffer_on;
					done = 1;
				}
				else{
					nal_bytes_to_write = buffer_size - buffer_on-5;
				}

				PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
						  &nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);
				bytes += buffer_on+nal_bytes_to_write;
				if (!done && buffer_on != (buffer_size-5)) {
					printf("error read NAL and write packet data (lt_write_data_pkts) buffer_on = %d\n", buffer_on);
					return(-1);
				}
				if (!done)
					buffer_on = 5;

				// repeat convert PES data stream to TS data stream until next start code marker
				while (!feof(m_file) && ret ==0){ //while for long PES slice
					// read next 64K data into buffer
					done = 0;
				    buffer_size = MAX_BUFFER - buffer_on;
				    memmove(buffer, &buffer[buffer_size], buffer_on);
					buffer_size = fread(&buffer[buffer_on], 1, buffer_size, m_file);

					if (crash_flag= test_end_of_file(buffer_size, buffer_on)){
						/* when there are remaining bytes in H264 buffer, copy them to TS buffer */
						if (buffer_size>0 && buffer_on==0){
							nal_bytes_to_write = buffer_size;
							PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
										&nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);
						}

						lt_write_ts_pkts(tmp_buf, pts_val, put_pts);
						return(1);
					}


					buffer_size += buffer_on;
					buffer_on = 0;
					ret = lt_get_next_start_code(buffer, buffer_size, crash_flag);

					if (ret == 0){ // no start marker is found, convert the remain data to TS format
						err_counter++;
						if (err_counter>3){
							printf("TSMaker: error counter = %d\n", err_counter);
							return(-1);
						}
						buffer_on = 0;
						if (feof(m_file)){
							done = 1;
							nal_bytes_to_write = buffer_size;
						}
						else{
							nal_bytes_to_write = buffer_size -5;
						}
						PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
								  &nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);

					}
					else { /* normal case */
						nal_bytes_to_write = ret;
						PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
								  &nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);
						break;
					} // end of else for normal case
				} // end of while for long PES slice
			} // end of if (ret == 0)
			else if (ret <=3){ // close file and return
				lt_write_ts_pkts(tmp_buf, pts_val, put_pts);
				printf("error read NAL and write packet data (lt_write_data_pkts) ret = %d\n",ret);
				return(-1);
			}
			else { // normal case
				prefix_buffer = buffer + buffer_on;
				nal_bytes_to_write = ret;
				PES_header_info(prefix_buffer, &first_time, tmp_buf, &buf_index,pts_val, put_pts, &VCL,
				&prev_PPS);

				prev_PPS--;
				PES_payload_data(buffer, &buffer_on, tmp_buf, &buf_index,
					  &nal_bytes_to_write, pts_base, &pts_val, &put_pts, VCL);
			}
		} while (done == 0); // end of "do"
	}

	lt_write_ts_pkts(tmp_buf, pts_val, put_pts);
	return(0);
}
#endif



/* ---------------------------------------------------------------
 * Write PES data to buffer without bit operation
 *
 *---------------------------------------------------------------*/

int	write_pes_buf(unsigned char *pes_buf, int *pes_index, int len)
{

	int buf_index = current_bit_pos>>3;
	if (buf_index*8 != current_bit_pos)
		return(1);

	if (buf_index + len >buf_len){
        lt_flush_buf();
		buf_index = 0;
	}
	memcpy(&buf[buf_index], &pes_buf[*pes_index],len);
	current_bit_pos += len*8;
	total_bits	+= len*8;
	*pes_index += len;

	return(0);
}

// write TS packets
#ifdef SINGLE_PES_PER_FRAME
void lt_write_ts_pkts(unsigned char *pes_buf, unsigned int length,
                      unsigned long pts_val)
#else
void lt_write_ts_pkts(unsigned char *pes_buf, unsigned long pts_val, int put_pts)
#endif
{
	int start_pes = 1;
	int pes_buf_index = 0;
	//static int cc = 0;
	int pts_dts_flags = 3;
	int i = 0;


#ifdef SINGLE_PES_PER_FRAME
	bytes_to_write = length;
#else
	bytes_to_write = PES_PAYLOAD_SIZE;

	if (put_pts)
		pts_dts_flags = 2;
	else
		pts_dts_flags = 0;
#endif

	while ( bytes_to_write > 0 ) {

		if (cc > 15)
			cc = 0;

		cur_ts_pkt_bytes = 0;
		if (start_pes) {
			lt_write_ts_header(1, ES_PID, 3, cc);
			lt_write_pes_header(pts_val, pts_dts_flags);
			start_pes = 0;
		}
		else
			lt_write_ts_header(0, ES_PID, 3, cc);
#if TSMAKER_ENHANCED
		if ( bytes_to_write  >= (188 - cur_ts_pkt_bytes) )   {
			if (write_pes_buf(pes_buf, &pes_buf_index, (188 - cur_ts_pkt_bytes))){
				for (i=0; i<188 - cur_ts_pkt_bytes; i++) {
					lt_write_bits(*(pes_buf + pes_buf_index), 8);
					pes_buf_index++;
				}
			}
			bytes_to_write -= (188 - cur_ts_pkt_bytes);
		}
		else {   //case if <=
			if (write_pes_buf(pes_buf, &pes_buf_index, bytes_to_write)){
				for (i=0; i<bytes_to_write; i++) {
					lt_write_bits(*(pes_buf + pes_buf_index), 8);
					pes_buf_index++;
				}
			}
     		bytes_to_write -= bytes_to_write;
		}
#else
		if ( bytes_to_write  >= (188 - cur_ts_pkt_bytes) )   {
			for (i=0; i<188 - cur_ts_pkt_bytes; i++) {
				lt_write_bits(*(pes_buf + pes_buf_index), 8);
			    pes_buf_index++;
			}
			bytes_to_write -= (188 - cur_ts_pkt_bytes);
		}
		else {   //case if <=
			for (i=0; i<bytes_to_write; i++) {
				lt_write_bits(*(pes_buf + pes_buf_index), 8);
			    pes_buf_index++;
			}
     		bytes_to_write -= bytes_to_write;
		}


#endif
		cc++;
	}
}



// get next code to indicate start of NAL
unsigned int lt_get_next_start_code (unsigned char *tmp_buffer, unsigned int buffer_size,unsigned int crash_flag)
{
  unsigned int value = 0xffffffff;
  unsigned int byte_offset = 0;

  if ( (tmp_buffer[0] == 0) && (tmp_buffer[1] == 0) && (tmp_buffer[2] == 0) && (tmp_buffer[3] == 1) ) {
	byte_offset = 4;
    tmp_buffer += 4;
  }
  else if ( (tmp_buffer[0] == 0) && (tmp_buffer[1] == 0) && (tmp_buffer[2] == 1) ) {
	byte_offset = 3;
    tmp_buffer += 3;
  }

  if (buffer_size<=3 || buffer_size>MAX_BUFFER || crash_flag){
    //DBG_PRINT(DBG_LEVEL_5, "#1 byte_offset buffer_size= %d %d  %d\n",crash_flag, byte_offset, buffer_size);
  }


  while (byte_offset < buffer_size - 3) {
    value <<= 8;
    value |= *tmp_buffer++;
    byte_offset++;

#if TSMAKER_ENHANCED
    if ((value & 0x00ffffff) == START_CODE_PREFIX) {
		if (value ==START_CODE_PREFIX) {
			if(crash_flag ==1){
                        //DBG_PRINT(DBG_LEVEL_5,"#8 crash flag %d , byte_offset-4 = %d  %d\n",crash_flag, byte_offset-4, buffer_size);
			}
			return byte_offset - 4;
        }
		if(crash_flag ==1){
                 //DBG_PRINT(DBG_LEVEL_5,"#9 crash flag %d , byte_offset-3 = %d  %d\n",crash_flag, byte_offset-4, buffer_size);
		}
		return byte_offset - 3;
    }

#else
    if (value == START_CODE_PREFIX) {
		if (buffer_size<=3 || buffer_size>MAX_BUFFER || crash_flag){
			//DBG_PRINT(DBG_LEVEL_5,"#2 crash flag %d , byte_offset-4 = %d  %d\n",crash_flag, byte_offset-4, buffer_size);
		}
      return byte_offset - 4;
    }

    if ((value & 0x00ffffff) == START_CODE_PREFIX) {

		if (buffer_size<=3 || buffer_size>MAX_BUFFER||crash_flag){
	//DBG_PRINT(DBG_LEVEL_5,"#3 crash flag %d byte_offset-3  buffer_size=%d   %d\n",crash_flag, byte_offset-3, buffer_size);
		}
      return byte_offset - 3;
    }
#endif
  }

  if (buffer_size<=3 || buffer_size>MAX_BUFFER ||crash_flag){
	//DBG_PRINT(DBG_LEVEL_5,"#4  crash_flag %d byte_offset buffer_size= %d   %d\n",crash_flag, byte_offset, buffer_size);
  }
  return 0;
}


// write time stamp values
void lt_write_pts(int fourbits, unsigned long long pts)
{
	unsigned long tmp_value = 0L;

	tmp_value = fourbits << 4 | (((pts >> 30) & 0x07) << 1) | 1;
	lt_write_bits(tmp_value, 8);
	tmp_value = (((pts >> 15) & 0x7fff) << 1) | 1;
	lt_write_bits(tmp_value, 16);
	tmp_value = ((pts & 0x7fff) << 1) | 1;
	lt_write_bits(tmp_value, 16);

}

// write bits into bitstream
int lt_write_bits(unsigned int bits, int n)
{
    int delta;		    // required input shift amount
    unsigned char *v;	// current byte
    unsigned int tmp;	// temp value for shifted bits

    last_bits = n;

    /* If number of bits in buffer is > the new total bits, flush buffer to file
     * buff_len << 3 = number of bits (can't just multiply by 8 because it's architecture specific)
     */
    if (current_bit_pos + n > (buf_len<<3))
        lt_flush_buf();

    delta=32-n-(current_bit_pos%8);
    v=buf+(current_bit_pos>>3);

    if (delta>=0) {
        tmp = bits<<delta;
		v[0] |= tmp>>24;
        v[1] |= tmp>>16;
        v[2] |= tmp>>8;
        v[3] |= tmp;
    } else {
        tmp = bits>>(-delta); // -delta<8
        v[0] |= tmp>>24;
        v[1] |= tmp>>16;
        v[2] |= tmp>>8;
        v[3] |= tmp;
        v[4] |= bits<<(8+delta);
    }

    current_bit_pos += n;
    total_bits += n;
    return bits;
}


// flushes rest of bits
void lt_flush_bits()
{
	//int tmp = 0;

    lt_flush_buf();

    if (current_bit_pos == 0)
        return;

    //tmp=write(fd, buf, 1);
    // vector output
    output_vect->insert(output_vect->end(), buf, buf + 1);

    //if (tmp!=1){
        //lt_set_error_code(ERR_WRITE_FAILED);
        //return;
    //}

    buf[0] = 0;
    current_bit_pos = 0;
}


// output the buffer excluding the left-over bits
void lt_flush_buf()
{
    int l;

    /* Bits to bytes */
    l = (current_bit_pos >> 3);

    // file output
    //n=write(fd, buf, l);
    // vector output
    output_vect->insert(output_vect->end(), buf, buf + l);

    if (current_bit_pos & 0x7) {
        buf[0] = buf[l];
        memset(buf+1, 0, TS_BUFFER_SIZE-1);
    }
    else {
        memset(buf, 0, TS_BUFFER_SIZE);
    }

    current_bit_pos &= 7;
}

// get frame rate
unsigned int lt_get_frame_rate()
{
	return frame_rate;
}

// sets error code
void lt_set_error_code(ERROR_TYPE err)
{
	ErrCode = err;
}

// get error code
int lt_get_error_code(void)
{
	return ErrCode;
}
