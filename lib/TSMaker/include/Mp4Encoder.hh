/*******************************************************************************
 * Copyright 2007 Cernium Corporation.  All Rights Reserved.
 *******************************************************************************
 *
 * Mp4Encoder.h
 *
 * This file contains extern declaration of variables, structure and definitions
 * used in Mp4Encoder.c
 * Version 1.2: Mp4 container for H.264 BP.
 *              Supports variable I frame interval.
 *              Supports P frames of zero size, but not I frames.
 * 
 ******************************************************************************/

#ifndef _MP4ENCODER_HH_
#define _MP4ENCODER_HH_

/**************************
 * Standard Linux headers *
 **************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <vector>

using namespace std;
/*******************
* Defines Symbols *
*******************/
#define TIME_SCALE 90000
#define MAX_H264_HEADER_SIZE	200
#define MP4HEADERSIZE			32
#define TREXSIZE                32
#define MEHDSIZE                16
#define MDATHEADERSIZE          8
#define MVEXHEADERSIZE          (TREXSIZE + 8)
#define MFHDSIZE                16
#define TFHDSIZE                24
#define TFDTSIZE                20
#define MP4_OK					0
#define MP4_ERROR				-1
#define MP4_NULLERRORSTRING		-2


// The size (bytes) of the Mp4 footer (if MOOV atom is at footer) is
// (640 + 12*MAX_VIDEO_FRAMES + 4*MAX_VIDEO_FRAMES/I_FRAME_INTERVAL)
// If the I_FRAME_INTERVAL is variable, specify the min. I_FRAME_INTERVAL

/**************************
 * Structure Definitions  *
 **************************/
typedef struct frameInfo
{
    unsigned startIndex;
	unsigned frameSize;
	char IFrame;		// 1: I/IDR frame. 0: P/B frame.
} frameInfo;

typedef struct VideoInfo
{
	int width;
	int height;
	int fps;
	int totalNumFrm;
	frameInfo *frameSizeArray;
	unsigned int *chunkOffsetTable;
	char *H264_Header;
	int H264HeaderSize;
	int iFrameInt;
} VideoInfo;

typedef struct AudioInfo
{
	int noOfChannels;
	int bitsPerSample;
	int sampleRate;
	int bitRate;
	int aacLC;
	frameInfo *frameSizeArray;
	unsigned int *chunkOffsetTable;
	int totalNumFrm;
} AudioInfo;


/***********************
 * Function Prototypes *
 **********************/
int getH264Header( const char *buffer, char *H264Header, int H264HeaderBufSize,
				   int *H264HeaderSize );
int getH264Header( const unsigned char *buffer, vector<char> &H264Header, int H264HeaderBufSize);

int generateMp4Header_IDR_Frame( char *buffer, int bufferSize );

int generateMp4Header_I_Frame( char *buffer, int bufferSize );

int generateMp4Footer( char *buffer, unsigned int *scratchTable, unsigned int scratchTableSize,
					   int *mSize, char *errorString, VideoInfo *videoInfo,
					   AudioInfo *audioInfo);
// MUST fopen the file in 'r+' or 'r+b' mode
int insertFrameSize( FILE *inFile, int mSize, char *errorString );

int generateMp4Header_w_moov_atom( FILE *fp, const int totalNumFrm,
								   const int iFrameInterval , const int numAudioFrames);
int generateMp4Header_w_moov_atom_w_spsppssize(vector<unsigned char> &output_vector, const int totalNumFrm,
				 		const int iFrameInterval , const int numAudioFrames,
						const int spsPpsSize);
int generateMp4Header_w_moov_atom_w_spsppssize( FILE *fp, const int totalNumFrm,
				 		const int iFrameInterval , const int numAudioFrames,
						const int spsPpsSize);

int writeMoovAtom( FILE *fp, char *moovBuffer, int moovBufSize,
				   int mp4HeaderSize, int mSize );
int writeMoovAtom(vector<unsigned char> &output_vector, char *moovBuffer, int moovBufSize,
				   int mp4HeaderSize, int mSize );

void writeFragment(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, VideoInfo *videoInfo, AudioInfo *audioInfo, unsigned seq_num, unsigned int *video_pts_index, unsigned int *audio_pts_index);
void writeMoof(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, VideoInfo
        *videoInfo, AudioInfo *audioInfo, unsigned seq_num, unsigned int *video_pts_index, unsigned int *audio_pts_index);
void writeMdat(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo
        *frame_size_array, unsigned frame_count);

int writeTraf(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo
        *frame_size_array, unsigned track_id, unsigned totalNumFrm, unsigned track_duration, unsigned long pts, unsigned int *pts_index);

void writeTfhd(vector<unsigned char> &output_vector, unsigned track_id, unsigned track_duration);
void writeTfdt(vector<unsigned char> &output_vector, unsigned long pts, unsigned int *pts_index);
int writeTrun(vector<unsigned char> &output_vector, vector<unsigned char> frame_vector, frameInfo
        *frame_size_array, unsigned sample_count);

int getFrameStartOffset(unsigned char *buffer, int length);


void writePlaceholderBytes(vector<unsigned char> &output_vector, size_t count);

void writeString(vector<unsigned char> &output_vector, const string value);

void numToBytes(char byteArray[4], unsigned num);
void longToBytes(char byteArray[8], unsigned long num);
void setBaseMediaDecodeTime(vector<unsigned char> &output_vector, unsigned long pts);

#endif //_MP4ENCODER_H_
