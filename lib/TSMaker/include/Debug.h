/******************************************************************************
 * Copyright 2007 Cernium Corporation.  All Rights Reserved.
 ******************************************************************************
 *
 * Debug.h
 *
 * This file is the header file for printing the debug informations.
 *
 *****************************************************************************/

#ifndef CERNIUM_DEBUG_H
#define CERNIUM_DEBUG_H

/*********************
 * Include Files
 *********************/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <syslog.h>

/***************************************************************************
 *                      Debug Levels' SECTION                           *
 **************************************************************************/

/*
 Total 6 debug levels from 0 to 5 are provided.
 CERNIUM_DEBUG_LEVEL_0 disables debugging messages.
 CERNIUM_DEBUG_LEVEL_N includes all previous debug levels.
 for example CERNIUM_DEBUG_LEVEL_3 includes CERNIUM_DEBUG_LEVEL_3, CERNIUM_DEBUG_LEVEL_2
 and CERNIUM_DEBUG_LEVEL_1.
 This way debug level 5 is the deepest debug level which includes all debug messages
 Default debug level is CERNIUM_DEBUG_LEVEL_0
 DEBUG_LEVEL_1 : Messages shows that a particular function is invoked and left.
 DEBUG_LEVEL_2 : Message prints values of all parameters passed to the function
 DEBUG_LEVEL_3 : Messages show the values of global variables that are used in
       multiple functions.
 DEBUG_LEVEL_4 : Messages show values returned from the system calls
 DEBUG_LEVEL_5 : Messages show acknowledgements or the activities going on.
*/


/* Debug levels supported */
#define DBG_LEVEL_1  1 /* Lowest debug level */
#define DBG_LEVEL_2  2
#define DBG_LEVEL_3  3
#define DBG_LEVEL_4  4
#define DBG_LEVEL_5  5
/*
Highest debug level, Enabling this enables all
lower debug levels from _LEVEL_4 to _LEVEL_1
*/

/* Debug print macro based on 'ENABLE_DBG_LEVEL' specified at compile time */
#if ENABLE_DBG_LEVEL > 0

/* Get and set debug levels at run-time */
#define GetDbgLevel() (dbgLevel)
#define SetDbgLevel(n) do{ dbgLevel = n; }while(0)

#define DBG_PRINT(level, format, args...) \
    do { \
        if ( level <= dbgLevel ) \
        { \
			syslog(LOG_DEBUG | LOG_USER," " format,  ##args);\
        } \
    } while(0)

#else
/* Debugging is turned OFF during compilation */

#define GetDbgLevel() 0
#define SetDbgLevel(n) do{}while(0)

#define DBG_PRINT(level, format, args...) do{}while(0)

#endif /* DBG_LEVEL_ENABLE */

/* Error message */
#define PRINT_ERR(format, args...) \
    syslog(LOG_ERR | LOG_USER,"!!!ERROR!!! " format,  ##args);\
//printf("!!!ERROR!!! "format, ##args)

/* Warning message */
#define PRINT_WARN(format, args...) \
	syslog(LOG_WARNING | LOG_USER,"WARNING: " format,  ##args);\
//printf("WARNING: "format, ##args)

/* Informational message */
#define PRINT_INFO(format, args...) \
    syslog(LOG_INFO | LOG_USER,"INFO: " format,  ##args);\
//printf("INFO: "format, ##args)

/**********
* Globals
**********/
extern unsigned int dbgLevel;

#endif /* _INCLUDE_DBG_PRINT_H */

/******************************** END OF FILE *********************************/

