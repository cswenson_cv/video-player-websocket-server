/*
 * H264_TS_Packetizer.h
 *
 * Copyright (c) 2009 Cernium Corporation
 * All Rights Reserved.
 *
 *
 * Author: Lai-Tee Cheok
 *
 */


#ifndef _h264_ts_packetizer_h_
#define _h264_ts_packetizer_h_

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <cstdint>
#include <vector>

#include "Mp4Encoder.hh"

#define DEFAULT_FRAME_RATE 30
#define FRAME_RATE 10
#define TSPACKETDURATION (FRAME_RATE * 2)
#define WIDTH 2688
#define HEIGHT 1512

#define PGM_NUM	1
#define PGM_MAP_PID 4095
#define PCR_PID 256
#define ES_PID 256
#define START_CODE_PREFIX 0x000001
#define MAX_BUFFER 65536
#define PTS_DTS_FLAGS 2

// TODO Make these specific to the strem
#define PES_PAYLOAD_SIZE 2930
#define TS_BUFFER_SIZE 1024

#define SINGLE_PES_PER_FRAME

// list of exceptions/errors
typedef enum {
    ERR_NONE,
    ERR_READ_FAILED,
    ERR_WRITE_FAILED
} ERROR_TYPE;

// buffer size
extern int current_bit_pos;
extern unsigned char buf[TS_BUFFER_SIZE+3];
extern int cur_ts_pkt_bytes;
extern FILE *m_file;
extern unsigned long bytes_to_write;
extern unsigned long pts_incr;
extern int first_ts_pkt;
extern int prev_PPS;
extern unsigned int frame_rate;
extern int fd;
extern int buf_len;
extern unsigned int total_bits;
extern unsigned int last_bits;
extern ERROR_TYPE ErrCode;
extern std::vector<uint8_t> *output_vect;


void lt_write_ts_header(int payload_indicator, int pid, int adapt_field_ctrl, int cc);
void lt_write_pes_header(unsigned long pts, unsigned int pts_dts_flags);
void lt_write_adaptation_field(int pgm_clk_ref);
void lt_write_PAT(int table_id, int pgm_num, int pgm_map_pid, int ts_id);
void lt_write_PMT(int table_id, int pgm_num, int pcr_pid, int es_pid);
void lt_stuff_bytes(int num_bytes);
//unsigned int lt_get_next_start_code(unsigned char *buf, unsigned int buf_len);
unsigned int lt_get_next_start_code (unsigned char *tmp_buffer, unsigned int buffer_size,unsigned int crash_flag);

void lt_write_pts(int fourbits, unsigned long long pts);
#ifdef SINGLE_PES_PER_FRAME
void lt_write_ts_pkts(unsigned char *pes_buf, unsigned int length,unsigned long pts_val);
#else
void lt_write_ts_pkts(unsigned char *pes_buf, unsigned long pts_val, int put_pts);
#endif
void lt_create_PAT();
void lt_create_PMT();
int lt_write_data_pkts();
int lt_write_data_pkts(unsigned char *buffer, unsigned int size);
unsigned int lt_get_frame_rate();
void lt_flush_buf();
void lt_set_error_code(ERROR_TYPE err);
int  lt_packetize();
int  lt_packetize(std::vector<uint8_t> frame_buffer, std::vector<struct frameInfo> frame_size_array, bool ts_headers);
int lt_frame_init();
int lt_init(std::vector<uint8_t> *output, int fr);
int lt_init(const char *m_file, const char *output_file, int frame_rate);
void lt_cleanup(void);
int lt_write_bits(unsigned int bits, int n);
void lt_flush_bits();
int lt_get_error_code(void);


#endif /*  _h264_ts_packetizer_h_ */
