#ifndef _BROADCASTSERVER_HH_
#define _BROADCASTSERVER_HH_

#include <set>
#include <thread>
#include <mutex>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>


#define CLOSE_MSG "Server shutting down"
typedef websocketpp::server<websocketpp::config::asio> server_t;

using websocketpp::connection_hdl;

using namespace std;

/**
 * Stores info about each connection, unused since clients maintain their own info, but can be used for future enhancements.
 */
class connection_info {
    private:
        unsigned long pts;
        unsigned long pts_incr;
        bool pts_set;
    public:
        connection_info() { this->pts = 0; this->pts_incr = 0; this->pts_set = false; };
        connection_info(unsigned long pts, unsigned long pts_incr) { this->pts = pts; this->pts_incr = pts_incr; this->pts_set = false; };
        unsigned long get_pts() { return this->pts; };
        void set_pts(unsigned long p) { this->pts = p; };
        void increment_pts() { this->pts += this->pts_incr; };
        void toggle_pts_set() { this->pts_set = !(this->pts_set); };
        bool is_pts_set() { return this->pts_set; };
};

typedef map<connection_hdl, connection_info *, owner_less<connection_hdl>> con_list;

/**
 * WebSocket server with messaging channel, stores uninitalized connections, separately until an initialization message is sent. Servers listen on separate threads.
 */
class BroadcastServer {
    public:
        BroadcastServer();
        ~BroadcastServer();
        /**
         * Constructor with stream info string
         *
         * @param str_info JSON string to send as first message
         * @param pts_incr (optional) pts_increment value to use
         */
        BroadcastServer(string str_info, unsigned long pts_incr = 0): BroadcastServer() { this->stream_info = string(str_info); this->pts_incr = pts_incr; };
        /**
         * Setter for stream_info_string
         *
         * @param str_info: JSON string to send as first message
         */
        void set_stream_info(string str_info) { this->stream_info = string(str_info); };
        /**
         * Runs the servers
         *
         * @param port port to run the control channel on
         */
        void run(unsigned port);


        /**
         * Broadcasts a message on either the control channel or video channel, if the mssage is an initialization message it will be sent to non-initalized clients
         *
         * @tparam T the type of data stored in payload
         * @param payload data to send
         * @param len payload length
         * @param control flag to indicate message is control message or not
         * @param op websocketpp message type (binary, text, etc.)
         * @param initial if the message is an initialization message
         * @param preprocess (optional) callback function used to preprocess messages per client
         */
        template <typename T>
            void broadcast_message(vector<T> payload, size_t len, bool control, websocketpp::frame::opcode::value op, bool initial, void (*preprocess)(vector<T> &, connection_info *&) = NULL);

        /**
         * Broadcasts a message on either the control channel or video channel, if the mssage is an initialization message it will be sent to non-initalized clients
         *
         * @tparam the type of data to store in a vector representation of payload
         * @tparam PType the type of data stored in payload
         * @param payload data to send
         * @param len payload length
         * @param control flag to indicate message is control message or not
         * @param op websocketpp message type (binary, text, etc.)
         * @param initial if the message is an initialization message
         * @param preprocess (optional) callback function used to preprocess messages per client
         */
        template <typename T, typename PType>
            void broadcast_message(PType const *payload, size_t len, bool control, websocketpp::frame::opcode::value op, bool initial, void (*preprocess)(vector<T> &, connection_info *&) = NULL);

        /**
         * Stops the servers
         */
        void stop();

        /*
         * Get the number of uninitialized connections
         *
         * @returns number of uninitialized connections
         */
        int num_non_init();

    private:
        const static string VIDEO_SETUP;
        const static string PORT_HEADER;

        /*
         * Threads to run the listen loop on
         */
        thread controlServerThread;
        thread serverThread;
        /*
         * Protects con_list shared data
         */
        mutex con_list_mutex;
        mutex control_con_list_mutex;

        /*
         * Protect video_port
         */
        mutex video_port_mutex;

        server_t m_server;
        server_t m_control_server;

        con_list m_control_connections;
        con_list m_connections;
        con_list non_init_cons;

        unsigned long pts_incr;
        unsigned int video_port;
        string video_port_message;
        string stream_info;

        /*
         * Runs a server, specified by control
         *
         * @param port port to run the server on
         * @param control which server to run
         */
        void run_server(unsigned port, bool control);

        /**
         * Callback that fires on connection open to the video server
         *
         * @param hdl weak_ptr to the connection
         */
        void on_open(connection_hdl hdl);
        
        /**
         * Callback that fires on connection close on the video server
         *
         * @param hdl weak_ptr to the connection
         */
        void on_close(connection_hdl hdl);

        /**
         * Callback that fires on connection open on the control server
         *
         * @param hdl weak_ptr to the connection
         */
        void on_control_open(connection_hdl hdl);

        /**
         * Callback that fires on connection close on the control server
         *
         * @param hdl weak_ptr to the connection
         */
        void on_control_close(connection_hdl hdl);

        /**
         * Sends a message to a connection, on fail, closes the invalid connection
         *
         * @param hdl weak_ptr to the connection
         * @param control flag to indicate message is control message or not
         * @param payload message data
         * @param len payload length
         * @param op websocketpp message type
         */
        bool send(connection_hdl hdl, bool control, void const *payload, size_t len, websocketpp::frame::opcode::value op);

        /**
         * Broadcasts a message on either the control channel or video channel, if the mssage is an initialization message it will be sent to non-initalized clients
         *
         * @tparam T the type of data stored in payload
         * @param payload data to send
         * @param len payload length
         * @param control flag to indicate message is control message or not
         * @param connection_list list to send the message to
         * @param op websocketpp message type (binary, text, etc.)
         * @param initial if the message is an initialization message
         * @param preprocess (optional) callback function used to preprocess messages per client
         */
        template <typename T> 
            void broadcast_message(vector<T> payload, size_t len, bool control, con_list connection_list, websocketpp::frame::opcode::value op, void (*preprocess)(vector<T> &, connection_info *&));
        
        /**
         * Closes all connections on both servers
         */
        void close_all();

        /**
         * Creates json message for video port info
         */
        void video_port_to_json();
};
#endif
