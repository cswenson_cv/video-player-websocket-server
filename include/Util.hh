#ifndef _UTIL_HH_
#define _UTIL_HH_

#include <initializer_list>
#include <string>
#include <utility>

using namespace std;

/**
 * Utility functions
 */
namespace util {
    /**
     * Creates a json string based on lists of key value pairs
     * 
     * @param[in, out] output will contain the output, must be an initialized string object
     * @param[in] str_arg_list key-value pairs of string values
     * @param[in] int_arg_list key-value pairs of integer values
     */
    void fill_json_str(string &output, initializer_list<pair<string, string>> str_arg_list = {}, initializer_list<pair<string, int>> int_arg_list = {});
}

#endif
