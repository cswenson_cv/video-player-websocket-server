
#ifndef _WEBSOCKETRTSPCLIENT_HH_
#define _WEBSOCKETRTSPCLIENT_HH_

#include <string>

#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "H264_TS_Packetizer.hh"
#include "StreamInfo.hh"

#define VERSION "1.0"
#define ELEMENTARY_FORMAT "H264"
#define TS_FORMAT "TS"
#define MP4_FORMAT "MP4"
#define PROFILE_LEVEL_SEARCH_TEXT "profile-level-id="
#define SDP_DELIMETER ';'

#define DEFAULT_FRAMES 1


/**
 * Contains the types of muxing options
 */
enum mux_opts {
    NONE,
    MP4,
    TS
};

/**
 * Parsed command line opts
 */
struct options {
    int opt_debug;
    bool opt_seq_num;
    unsigned opt_port;
    unsigned opt_buffsize;
    unsigned opt_buffer_frames;
    enum mux_opts opt_mux;
    std::string opt_prog_name;
};


extern struct options opts;
extern StreamInfo stream_info;


// RTSP 'response handlers':
void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString);
void parseSdpString(char *const sdpString);

// Other event handler functions:
void subsessionAfterPlaying(void* clientData); // called when a stream's subsession (e.g., audio or video substream) ends
void subsessionByeHandler(void* clientData, char const* reason);
// called when a RTCP "BYE" is received for a subsession
void streamTimerHandler(void* clientData);
// called at the end of a stream's expected duration (if the stream has not already signaled its end using a RTCP "BYE")

// The main streaming routine (for each "rtsp://" URL):
void openURL(UsageEnvironment& env, char const* progName, char const* rtspURL);

// Used to iterate through each stream's 'subsessions', setting up each one:
void setupNextSubsession(RTSPClient* rtspClient);

// Used to shut down and close a stream (including its "RTSPClient" object):
void shutdownStream(RTSPClient* rtspClient, int exitCode = 1);
// Print usage message
void usage(UsageEnvironment& env, char const *progName);

// Standard getopt loop
void parse_args(UsageEnvironment *env, int argc, char **argv);


#endif
