#ifndef _STREAM_INFO_HH_
#define _STREAM_INFO_HH_

#include <string>

/**
 * Stores information about the WebSocket stream to send to clients
 */
class StreamInfo {
    private: 
        /**
         * Constants used for JSON conversion
         */
        static const std::string HEIGHT_HEADER;
        static const std::string STREAM_INFO_MESSAGE;
        static const std::string STREAM_FPS_HEADER;
        static const std::string PACKET_DURATION_HEADER;
        static const std::string PACKET_SEQUENCE_HEADER;
        static const std::string PROFILE_LEVEL_HEADER;
        static const std::string STREAM_FORMAT_HEADER;
        static const std::string STREAM_URL_HEADER;
        static const std::string WIDTH_HEADER;

        bool packet_sequence;
        unsigned int height;
        unsigned int packet_duration;
        unsigned int stream_fps;
        unsigned int width;
        std::string stream_url;
        std::string stream_format;
        std::string profile_level;

    public:
        static const std::string MESSAGE_TYPE_HEADER;

        /**
         * Standard getters/setters
         */
        unsigned int get_height() { return this->height; };
        unsigned int get_fps() { return this->stream_fps; };
        unsigned int get_packet_duration() { return this->packet_duration; };
        unsigned int get_width() { return this->width; };
        void set_packet_sequence(bool ps) { this->packet_sequence = ps; };
        void set_height (unsigned h) { this->height = h; };
        void set_packet_duration(unsigned pd) { this->packet_duration = pd; };
        void set_fps(unsigned f) { this->stream_fps = f; };
        void set_width(unsigned w) { this->width = w; };
        void set_stream_url(const std::string url) { this->stream_url = std::string(url); };
        void set_stream_format(const std::string format) { this->stream_format = std::string(format); };
        void set_profile_level(const std::string profile_level) { this->profile_level = std::string(profile_level); };


        /**
         * Fills output with the JSON representation of this object
         *
         * @param[in,out] output initialized string that will contain the output
         */
        void fill_json_str(std::string &output);
};

#endif
