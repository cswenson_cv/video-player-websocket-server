#ifndef _WEBSOCKETSINK_HH_
#define _WEBSOCKETSINK_HH_

#include <vector>

#include "liveMedia.hh"
#include "Mp4Encoder.hh"
#include "BasicUsageEnvironment.hh"
#include "BroadcastServer.hh"

#define DEFAULT_PORT 8765
#define DEFAULT_BUFFSIZE 2000000
#define BUFFSIZE 1000

// IDR Frame : & 1st byte with 1F == 5
/**
 * Custom sink which broadcasts frame data onto websocket and performs muxing
 */
class WebSocketSink: public MediaSink {
    public:
        static WebSocketSink* createNew(UsageEnvironment& env,
                MediaSubsession& subsession, // identifies the kind of data that's being received
                char const* streamId);

    private:
        WebSocketSink(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId);
        // called only by "createNew()"
        virtual ~WebSocketSink();

        /**
         * Live555 Callbacks that fire after a full frame is stored in fReceiveBuffer
         */
        static void afterGettingFrame(void* clientData, unsigned frameSize,
                unsigned numTruncatedBytes,
                struct timeval presentationTime,
                unsigned durationInMicroseconds);
        void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                struct timeval presentationTime, unsigned durationInMicroseconds);

        /**
         * Initializes BroadcastServer object used to send WebSocket messages
         */
        void createWebSocket();

        /**
         * Generates the MP4Header, should be called on every iframe
         */
        bool genMP4Header();

        /*
         * Boxes and sends data stored in frame_vector
         *
         * @returns bool if send was successful
         */
        bool sendData();

        /*
         * Copies data to frame_vector
         *
         * @param frameSize the size of the frame
         */
        void genChunks(unsigned frameSize);

        /**
         * Initializes the TS Muxer library
         */
        void createTSMuxer();
        
        /**
         * Boxes data stored in frame_vector as ts into sendChunks
         *
         * @param ts_headers if the output should contain the TS headers
         */
        void ts_box(bool ts_headers);

        /**
         * Boxes data stored in frame_vector as fragmented mp4 into sendChunks
         */
        void mp4_box();

        /**
         * Checks if a frame is an idr frame
         * 
         * @param frame_data full frame with AUD and start code
         * @returns true if frame is idr, false if not
         */
        bool is_idr(unsigned char *frame_data);

        /**
         * Checks if there is an idr present in the current buffer
         *
         * @returns true if there is an idr, false if not
         */
        bool check_idr_present();

    private:
        // redefined virtual functions:
        virtual Boolean continuePlaying();

    private:
        const static unsigned char START_CODE[];
        const static unsigned char END_OF_SEQUENCE[];
        const static unsigned START_CODE_LEN = 4;
        const static unsigned END_OF_SEQUENCE_LEN = 1;
        const static string PACKET_INFO;
        const static string SEQUENCE_NUMBER_HEADER;

        BroadcastServer *m_server;
        unsigned numFrames;
        unsigned iFrameCount;
        unsigned char* fReceiveBuffer;

        /*
         * Used only for debugging
         */
        FILE *file;

        /**
         * All vectors are class variables so they do not have to be resized with every frame, since vectors keep their underlying capacity after a clear operation.
         */

        /**
         * Stores output data after boxing
         */
        vector<unsigned char> sendChunks;

        /**
         * Stores current frame data
         */
        vector<unsigned char> frame_vector;
        vector<unsigned char> temp_frame_vector;

        /**
         * Stores mp4header
         */
        vector<unsigned char> headerBuffer;
        vector<char> H264Header;

        /**
         * Stores metadata about each frame (see MP4Lib for frameInfo details
         */
        vector <struct frameInfo> frame_size_array;

        /*
         * Packet sequence number
         */
        unsigned char packet_num;

        /*
         * Used by Live555
         */
        MediaSubsession& fSubsession;
        char* fStreamId;
};
#endif
